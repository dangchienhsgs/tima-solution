# Tima Solution


### Build
- `ssh -L 1433:115.84.179.3:1433 chiennd@139.59.237.212`
- `gradle build`

### Start project
- `gradle bootRun`

### Upload to server
- `bash bin/update_to_server.sh`

Login to server `146.196.65.192`

- `cd /home/chiennd/projects/tima-solution`
- `./runservice restart`