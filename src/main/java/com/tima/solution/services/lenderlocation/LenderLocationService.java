package com.tima.solution.services.lenderlocation;

import com.tima.solution.entities.mssql.TblLenderLocationEntity;

import java.util.List;

public interface LenderLocationService {

    List<TblLenderLocationEntity> getLenderAcceptLocation(int cityId, int districtId);

}
