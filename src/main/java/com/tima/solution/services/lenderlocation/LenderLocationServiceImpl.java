package com.tima.solution.services.lenderlocation;

import com.tima.solution.entities.mssql.TblLenderLocationEntity;
import com.tima.solution.repositories.mssql.LenderLocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LenderLocationServiceImpl implements LenderLocationService {
    private LenderLocationRepository repository;

    @Override
    public List<TblLenderLocationEntity> getLenderAcceptLocation(int cityId, int districtId) {
        return repository.findAllByCityIdAndDistrictId(cityId, districtId);
    }

    @Autowired
    public void setRepository(LenderLocationRepository repository) {
        this.repository = repository;
    }
}
