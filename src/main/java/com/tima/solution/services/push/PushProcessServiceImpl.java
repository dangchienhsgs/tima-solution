package com.tima.solution.services.push;

import com.tima.solution.entities.mssql.TblPushProcessEntity;
import com.tima.solution.entities.mssql.TblPushProcessEntityPK;
import com.tima.solution.repositories.mssql.TblPushProcessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class PushProcessServiceImpl implements PushProcessService {

    private TblPushProcessRepository repository;

    @Override
    public boolean isPushed(int lender, int loan) {
        return repository.findOne(new TblPushProcessEntityPK(loan, lender)) != null;
    }

    @Override
    public Set<Integer> getLenderPushByLoan(int loan) {
        Set<Integer> lenders = new HashSet<>();

        for (TblPushProcessEntity entity : repository.findAllByLoanId(loan)) {
            lenders.add(entity.getLenderId());
        }

        return lenders;
    }

    @Autowired
    public void setRepository(TblPushProcessRepository repository) {
        this.repository = repository;
    }
}
