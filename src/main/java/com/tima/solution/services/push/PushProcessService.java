package com.tima.solution.services.push;

import java.util.Set;

public interface PushProcessService {
    boolean isPushed(int lender, int loan);

    Set<Integer> getLenderPushByLoan(int loan);
}
