package com.tima.solution.services.customer;

import com.tima.solution.entities.mssql.TblCustomerCreditEntity;
import com.tima.solution.repositories.mssql.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository repository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository repository) {
        this.repository = repository;
    }

    @Override
    public TblCustomerCreditEntity findById(int id) {
        return repository.findOne(id);
    }
}
