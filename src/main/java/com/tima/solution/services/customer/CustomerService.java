package com.tima.solution.services.customer;

import com.tima.solution.entities.mssql.TblCustomerCreditEntity;

public interface CustomerService {
    TblCustomerCreditEntity findById(int id);
}
