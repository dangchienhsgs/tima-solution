package com.tima.solution.services.lender;


import com.tima.solution.configs.Constants;
import com.tima.solution.entities.mssql.TblLenderEntity;
import com.tima.solution.repositories.mssql.LenderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Service
public class LenderServiceImpl implements LenderService {
    private static final Logger logger = Logger.getLogger(LenderServiceImpl.class.getName());

    private LenderRepository lenderRepository;

    private TblLenderEntity tima;

    @Autowired
    public LenderServiceImpl(LenderRepository lenderRepository) {
        this.lenderRepository = lenderRepository;
        tima = lenderRepository.findOne(1);
    }

    @Override
    public List<TblLenderEntity> getLenderById(List<Integer> lenders) {
        return lenderRepository.findByIdIn(lenders);
    }

    @Override
    public TblLenderEntity getTimaLender() {
        return tima;
    }
}
