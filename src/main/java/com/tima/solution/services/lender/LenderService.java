package com.tima.solution.services.lender;

import com.tima.solution.entities.mssql.TblLenderEntity;

import java.util.List;

public interface LenderService {
    TblLenderEntity getTimaLender();

    List<TblLenderEntity> getLenderById(List<Integer> lenders);
}
