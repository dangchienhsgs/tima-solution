package com.tima.solution.services.user;

import java.util.Map;

public interface LogOnAppService {
    Map<Integer, Long> getLogOnAppMap();
}
