package com.tima.solution.services.user;

import com.tima.solution.entities.mssql.TblLogOnAppEntity;
import com.tima.solution.repositories.mssql.LogOnAppRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Service
public class LogOnAppServiceImpl implements LogOnAppService {
    private static Logger logger = Logger.getLogger(LogOnAppServiceImpl.class.getName());

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");

    private LogOnAppRepository repository;

    @Autowired
    public LogOnAppServiceImpl(LogOnAppRepository repository) {
        this.repository = repository;
    }

    @Override
    public Map<Integer, Long> getLogOnAppMap() {
        Calendar calendar = Calendar.getInstance();

        Date to = calendar.getTime();
        calendar.add(Calendar.DATE, -30);
        Date from = calendar.getTime();

        Iterable<TblLogOnAppEntity> entities = repository.getLogOnApp(from, to);


        Map<Integer, Long> result = new HashMap<>();

        for (TblLogOnAppEntity entity : entities) {
            int user = entity.getUserId();
            long timestamp = entity.getCreateDate().getTime();

            if (!result.containsKey(user)) {
                result.put(user, timestamp);
                continue;
            }

            if (result.get(user) < timestamp) {
                result.put(user, timestamp);
            }
        }

        logger.info("Load log on app time newest of " + result.size() + " users");
        return result;
    }
}
