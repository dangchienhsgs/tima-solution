package com.tima.solution.services.product;

import com.tima.solution.entities.mssql.TblProductCreditEntity;

import java.util.Map;

public interface ProductService {
    Map<Integer, TblProductCreditEntity> getProducts();
}
