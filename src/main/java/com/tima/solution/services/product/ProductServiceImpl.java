package com.tima.solution.services.product;

import com.tima.solution.entities.mssql.TblProductCreditEntity;
import com.tima.solution.repositories.mssql.ProductCreditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Service
public class ProductServiceImpl implements ProductService {
    private static Logger logger = Logger.getLogger(ProductServiceImpl.class.getName());

    private ProductCreditRepository repository;


    @Override
    public Map<Integer, TblProductCreditEntity> getProducts() {
        Map<Integer, TblProductCreditEntity> products = new HashMap<>();

        for (TblProductCreditEntity entity : repository.findAll()) {
            products.put(entity.getId(), entity);
        }

        return products;
    }

    @Autowired
    public void setRepository(ProductCreditRepository repository) {
        this.repository = repository;
    }
}
