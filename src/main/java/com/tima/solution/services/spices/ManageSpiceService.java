package com.tima.solution.services.spices;

import java.util.Set;

public interface ManageSpiceService {
    Set<Integer> getType(int lender);

    boolean isLenderAcceptProductId(int lenderId, int productId);
}
