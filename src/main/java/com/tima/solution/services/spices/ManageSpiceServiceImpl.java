package com.tima.solution.services.spices;

import com.tima.solution.entities.mssql.TblManageSpicesEntity;
import com.tima.solution.entities.mssql.TblProductCreditEntity;
import com.tima.solution.repositories.mssql.ManageSpiceRepository;
import com.tima.solution.services.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.logging.Logger;

@Service
public class ManageSpiceServiceImpl implements ManageSpiceService {
    private static Logger logger = Logger.getLogger(ManageSpiceServiceImpl.class.getName());

    private ManageSpiceRepository repository;

    private Map<Integer, Set<Integer>> lenderTypeMap;

    private ProductService productService;

    private Map<Integer, TblProductCreditEntity> products;

    @Autowired
    public ManageSpiceServiceImpl(ManageSpiceRepository repository, ProductService productService) {
        this.repository = repository;
        this.productService = productService;

        this.lenderTypeMap = loadLenderTypeMap();
        loadProduct();

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                logger.info("Reload type of lender map...");
                lenderTypeMap = loadLenderTypeMap();
            }
        };

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 1000 * 60 * 30, 1000 * 3600 * 2);
    }

    private void loadProduct() {
        products = productService.getProducts();
        logger.info("Load " + products.size() + " products");
    }

    private Map<Integer, Set<Integer>> loadLenderTypeMap() {
        Map<Integer, Set<Integer>> lenderTypeMap = new HashMap<>();

        logger.info("Load lender type map...");
        for (TblManageSpicesEntity spicesEntity : repository.findAll()) {
            int lender = spicesEntity.getLenderId();
            if (!lenderTypeMap.containsKey(lender)) {
                lenderTypeMap.put(lender, new HashSet<>());
            }

            lenderTypeMap.get(lender).add(spicesEntity.getProductId());
        }

        logger.info("Load type of " + lenderTypeMap.size() + " lenders");
        return lenderTypeMap;
    }

    @Override
    public Set<Integer> getType(int lender) {
        if (lenderTypeMap.containsKey(lender)) {
            return lenderTypeMap.get(lender);
        }

        logger.info("Lender " + lender + " is not in cached map type, return all types");
        return products.keySet();
    }

    public boolean isLenderAcceptProductId(int lenderId, int productId) {
        return getType(lenderId).contains(productId);
    }

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }
}
