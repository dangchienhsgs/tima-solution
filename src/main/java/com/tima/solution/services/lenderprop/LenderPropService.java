package com.tima.solution.services.lenderprop;

import com.tima.solution.entities.postgresql.LenderProperties;

import java.util.Map;

public interface LenderPropService {

    Map<Integer, LenderProperties> getLenderProps();

    LenderProperties getLenderProp(int lenderId);
}
