package com.tima.solution.services.lenderprop;

import com.tima.solution.entities.postgresql.LenderProperties;
import com.tima.solution.repositories.postgres.LenderPropRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class LenderPropServiceImpl implements LenderPropService {

    private LenderPropRepository repository;

    private Map<Integer, LenderProperties> lenderPropsEntityMap;

    @Autowired
    public LenderPropServiceImpl(LenderPropRepository repository) {
        this.repository = repository;

        lenderPropsEntityMap = load();
    }

    private Map<Integer, LenderProperties> load() {
        Map<Integer, LenderProperties> entityMap = new HashMap<>();
        for (LenderProperties entity : repository.findAll()) {
            entityMap.put(entity.getId(), entity);
        }

        return entityMap;
    }

    @Override
    public Map<Integer, LenderProperties> getLenderProps() {
        return lenderPropsEntityMap;
    }

    @Override
    public LenderProperties getLenderProp(int lenderId) {
        return lenderPropsEntityMap.get(lenderId);
    }
}
