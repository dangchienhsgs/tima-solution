package com.tima.solution.services.lenderprop;

public enum LenderType {
    GOOD_AND_ACTIVE,
    GOOD_AND_INACTIVE,
    BAD_AND_ACTIVE,
    BAD_AND_INACTIVE,
    NEW,
}
