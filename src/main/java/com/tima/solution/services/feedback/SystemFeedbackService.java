package com.tima.solution.services.feedback;

import com.tima.solution.entities.postgresql.SystemFeedbackEntity;

import java.util.List;

public interface SystemFeedbackService {
    void checkFilterLocationAndType(int loanId, int size, int cityId, int districtId, int type);

    void checkHasPriceLender(int loanId, int size, int cityId, int districtId, int type);

    void checkHasNoPriceLender(int loanId, int size, int cityId, int districtId, int type);

    List<SystemFeedbackEntity> findByCityId(int city);

    List<SystemFeedbackEntity> findByDistrictId(int district);
}
