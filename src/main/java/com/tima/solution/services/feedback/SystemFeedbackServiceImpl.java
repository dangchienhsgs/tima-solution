package com.tima.solution.services.feedback;

import com.tima.solution.configs.DistributionConfig;
import com.tima.solution.entities.postgresql.SystemFeedbackEntity;
import com.tima.solution.exception.Error;
import com.tima.solution.repositories.postgres.SystemFeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

@Service
public class SystemFeedbackServiceImpl implements SystemFeedbackService {
    private static Logger logger = Logger.getLogger(SystemFeedbackServiceImpl.class.getName());

    private SystemFeedbackRepository repository;

    private DistributionConfig distributionConfig;

    @Override
    public void checkFilterLocationAndType(int loan, int size, int cityId, int districtId, int type) {
        SystemFeedbackEntity entity = new SystemFeedbackEntity();
        entity.setCityid(cityId);
        entity.setDistrictid(districtId);
        entity.setCreateddate(new Timestamp(Calendar.getInstance().getTime().getTime()));
        entity.setLoanid(loan);

        if (size == 0) {
            entity.setErrorcode(Error.ZERO_LENDER_BY_LOCATION_AND_TYPE.getCode());
            entity.setPriority(Error.ZERO_LENDER_BY_LOCATION_AND_TYPE.getPriority());
            String msg = "No lender with type=" + type;
            entity.setMsg("Loan " + loan + ": " + msg);
            entity.setId(entity.hashCode());
            repository.save(entity);
            logger.info(msg);
            return;
        }


        if (size < distributionConfig.getConfig().getNumLenderPerTime()) {
            entity.setErrorcode(Error.NOT_ENOUGH_LENDER_BY_LOCATION_AND_TYPE.getCode());
            entity.setPriority(Error.NOT_ENOUGH_LENDER_BY_LOCATION_AND_TYPE.getPriority());
            String msg = "Not enough lender type=" + type + ": " + size + " < " + distributionConfig.getConfig().getNumLenderPerTime();
            entity.setId(entity.hashCode());
            entity.setMsg(msg);
            logger.info("Loan " + loan + ": " + msg);
            repository.save(entity);
        }
    }

    @Override
    public void checkHasPriceLender(int loanId, int size, int cityId, int districtId, int type) {
        SystemFeedbackEntity entity = new SystemFeedbackEntity();
        entity.setCityid(cityId);
        entity.setDistrictid(districtId);
        entity.setCreateddate(new Timestamp(Calendar.getInstance().getTime().getTime()));
        entity.setLoanid(loanId);
        entity.setId(entity.hashCode());

        if (size < distributionConfig.getConfig().getNumLenderPerTime()) {
            entity.setErrorcode(Error.NOT_ENOUGH_HAVE_MONEY_LENDER.getCode());
            entity.setPriority(Error.NOT_ENOUGH_HAVE_MONEY_LENDER.getPriority());

            String msg = "Not enough has money lender: " + size + " < " + distributionConfig.getConfig().getNumLenderPerTime();
            entity.setMsg(msg);
            logger.info("Loan " + loanId + ": " + msg);
            entity.setId(entity.hashCode());
            repository.save(entity);
        }
    }

    @Override
    public void checkHasNoPriceLender(int loanId, int size, int cityId, int districtId, int type) {
        SystemFeedbackEntity entity = new SystemFeedbackEntity();
        entity.setCityid(cityId);
        entity.setDistrictid(districtId);
        entity.setCreateddate(new Timestamp(Calendar.getInstance().getTime().getTime()));
        entity.setLoanid(loanId);

        if (size < distributionConfig.getConfig().getNumLenderPerTime()) {
            entity.setErrorcode(Error.NOT_ENOUGH_NO_MONEY_LENDER.getCode());
            entity.setPriority(Error.NOT_ENOUGH_NO_MONEY_LENDER.getPriority());

            String msg = "Not enough no money lender: " + size + " < " + distributionConfig.getConfig().getNumLenderPerTime();
            entity.setMsg(msg);
            logger.info("Loan " + loanId + ": " + msg);
            entity.setId(entity.hashCode());
            repository.save(entity);
        }
    }


    @Override
    public List<SystemFeedbackEntity> findByCityId(int city) {
        return repository.findAllByCityidOrderByCreateddate(city);
    }

    @Override
    public List<SystemFeedbackEntity> findByDistrictId(int district) {
        return repository.findAllByDistrictidOrderByCreateddate(district);
    }

    @Autowired
    public void setRepository(SystemFeedbackRepository repository) {
        this.repository = repository;
    }

    @Autowired
    public void setDistributionConfig(DistributionConfig distributionConfig) {
        this.distributionConfig = distributionConfig;
    }
}
