package com.tima.solution.services.feedback;

public class ReasonBuilder {

    public String getFeedbackNotEnoughLender(int cityId, int districtId, int type, int size) {
        if (size == 0) {
            return getFeedbackNoLender(cityId, districtId, type);
        } else {
            return "Not enough lender in cityId=" + cityId + ", districtId=" + districtId + ", type=" + type;
        }
    }

    public String getFeedbackNoLender(int cityId, int districtId, int type) {
        return "No lender in cityId=" + cityId + ", districtId=" + districtId + ", type=" + type;
    }
}
