package com.tima.solution.services.loan;

import com.tima.solution.entities.mssql.TblLoanCreditEntity;

public interface LoanService {
    TblLoanCreditEntity findLoanById(int id);
}
