package com.tima.solution.services.loan;


import com.tima.solution.entities.mssql.TblLoanCreditEntity;
import com.tima.solution.repositories.mssql.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoanServiceImpl implements LoanService {
    private LoanRepository loanRepository;

    @Autowired
    public void setLoanRepository(LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }

    @Override
    public TblLoanCreditEntity findLoanById(int id) {
        return loanRepository.findOne(id);
    }
}
