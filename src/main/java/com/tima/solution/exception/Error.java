package com.tima.solution.exception;

import com.tima.solution.configs.Constants;

public enum Error {

    NOT_ENOUGH_NO_MONEY_LENDER(300, Constants.PRIORITY_MEDIUM),
    NOT_ENOUGH_HAVE_MONEY_LENDER(301, Constants.PRIORITY_HIGH),
    ZERO_LENDER_BY_LOCATION_AND_TYPE(302, Constants.PRIORITY_HIGH),
    NOT_ENOUGH_LENDER_BY_LOCATION_AND_TYPE(303, Constants.PRIORITY_HIGH),
    UNKNOWN_ERROR(404, Constants.PRIORITY_HIGH)
    ;

    int code;
    int priority;


    Error(int code, int priority) {
        this.code = code;
        this.priority = priority;
    }

    public int getCode() {
        return code;
    }

    public int getPriority() {
        return priority;
    }
}
