package com.tima.solution.model.response;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LenderDetail {
    private int lenderId;

    private int cityId;

    private int districtId;

    private boolean isActive;

    private boolean isGood;

    private int a7Rate;

    private int a3Rate;

    private double acceptRate;

    private String type;

    private long totalMoney;

    private String createdDate;

    public LenderDetail() {
    }


    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy");
        this.createdDate = dateFormat.format(createdDate);
    }

    public String getType() {
        return type;
    }

    public long getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(long totalMoney) {
        this.totalMoney = totalMoney;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLenderId() {
        return lenderId;
    }

    public void setLenderId(int lenderId) {
        this.lenderId = lenderId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isGood() {
        return isGood;
    }

    public void setGood(boolean good) {
        isGood = good;
    }

    public int getA7Rate() {
        return a7Rate;
    }

    public void setA7Rate(int a7Rate) {
        this.a7Rate = a7Rate;
    }

    public int getA3Rate() {
        return a3Rate;
    }

    public void setA3Rate(int a3Rate) {
        this.a3Rate = a3Rate;
    }

    public double getAcceptRate() {
        return acceptRate;
    }

    public void setAcceptRate(double acceptRate) {
        this.acceptRate = acceptRate;
    }
}
