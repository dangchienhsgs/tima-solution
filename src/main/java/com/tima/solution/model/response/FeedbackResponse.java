package com.tima.solution.model.response;

import com.tima.solution.entities.postgresql.SystemFeedbackEntity;

import java.util.List;

public class FeedbackResponse extends ApiMessage {

    private List<SystemFeedbackEntity> feedbacks;

    private int cityId;

    private int districtId;

    public FeedbackResponse(String message, List<SystemFeedbackEntity> feedbackEntities, int cityId, int districtId) {
        super(message);
        this.feedbacks = feedbackEntities;
        this.cityId = cityId;
        this.districtId = districtId;
    }

    public void setFeedbackEntities(List<SystemFeedbackEntity> feedbackEntities) {
        this.feedbacks = feedbackEntities;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public List<SystemFeedbackEntity> getFeedbackEntities() {
        return feedbacks;
    }

    public int getCityId() {
        return cityId;
    }

    public int getDistrictId() {
        return districtId;
    }
}
