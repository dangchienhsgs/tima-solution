package com.tima.solution.model.response;

public class ApiMessage {
    private String message;

    private boolean isError = false;


    public ApiMessage(String message) {
        this.message = message;
    }

    public ApiMessage(String message, boolean isError) {
        this.message = message;
        this.isError = isError;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }
}
