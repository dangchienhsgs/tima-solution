package com.tima.solution.model.response;

import com.tima.solution.entities.mssql.TblLoanCreditEntity;

import java.util.List;

public class DistributionResponse extends ApiMessage {
    private List<LenderDetail> data;
    private TblLoanCreditEntity loan;


    public DistributionResponse(String message, boolean isError, List<LenderDetail> data, TblLoanCreditEntity loan) {
        super(message, isError);
        this.data = data;
        this.loan = loan;
    }

    public TblLoanCreditEntity getLoan() {
        return loan;
    }

    public DistributionResponse(String message) {
        super(message);
    }

    public void setLoan(TblLoanCreditEntity loan) {
        this.loan = loan;
    }

    public List<LenderDetail> getData() {
        return data;
    }

    public void setData(List<LenderDetail> data) {
        this.data = data;
    }
}
