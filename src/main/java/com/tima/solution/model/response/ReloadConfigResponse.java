package com.tima.solution.model.response;

import com.tima.solution.entities.postgresql.DistributionConfigEntity;

public class ReloadConfigResponse extends ApiMessage {
    private DistributionConfigEntity config;

    public ReloadConfigResponse(String message, DistributionConfigEntity config) {
        super(message);
        this.config = config;
    }

    public ReloadConfigResponse(String message, boolean isError, DistributionConfigEntity config) {
        super(message, isError);
        this.config = config;
    }

    public DistributionConfigEntity getConfig() {
        return config;
    }

    public void setConfig(DistributionConfigEntity config) {
        this.config = config;
    }
}
