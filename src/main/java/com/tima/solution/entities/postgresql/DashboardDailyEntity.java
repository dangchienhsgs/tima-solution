package com.tima.solution.entities.postgresql;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "dashboard_daily", schema = "public", catalog = "postgres")
public class DashboardDailyEntity {
    private Date date;
    private Integer totalLender;
    private Integer totalBorrower;

    @Id
    @Column(name = "date")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Basic
    @Column(name = "total_lender")
    public Integer getTotalLender() {
        return totalLender;
    }

    public void setTotalLender(Integer totalLender) {
        this.totalLender = totalLender;
    }

    @Basic
    @Column(name = "total_borrower")
    public Integer getTotalBorrower() {
        return totalBorrower;
    }

    public void setTotalBorrower(Integer totalBorrower) {
        this.totalBorrower = totalBorrower;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DashboardDailyEntity that = (DashboardDailyEntity) o;
        return Objects.equals(date, that.date) &&
                Objects.equals(totalLender, that.totalLender) &&
                Objects.equals(totalBorrower, that.totalBorrower);
    }

    @Override
    public int hashCode() {

        return Objects.hash(date, totalLender, totalBorrower);
    }
}
