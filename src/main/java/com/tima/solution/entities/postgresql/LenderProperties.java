package com.tima.solution.entities.postgresql;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tima_lender_props", schema = "public", catalog = "postgres")
public class LenderProperties {
    private int id;
    private Integer a7Rate;
    private Integer numAcceptLoan;
    private Integer numPushedLoan;
    private Double acceptRate;
    private Long maxAcceptValue;
    private Integer numSysRejectLoan;
    private Integer a3Rate;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "a7_rate")
    public Integer getA7Rate() {
        return a7Rate;
    }

    public void setA7Rate(Integer a7Rate) {
        this.a7Rate = a7Rate;
    }

    @Basic
    @Column(name = "num_accept_loan")
    public Integer getNumAcceptLoan() {
        return numAcceptLoan;
    }

    public void setNumAcceptLoan(Integer numAcceptLoan) {
        this.numAcceptLoan = numAcceptLoan;
    }

    @Basic
    @Column(name = "num_pushed_loan")
    public Integer getNumPushedLoan() {
        return numPushedLoan;
    }

    public void setNumPushedLoan(Integer numPushedLoan) {
        this.numPushedLoan = numPushedLoan;
    }

    @Basic
    @Column(name = "accept_rate")
    public Double getAcceptRate() {
        return acceptRate;
    }

    public void setAcceptRate(Double acceptRate) {
        this.acceptRate = acceptRate;
    }

    @Basic
    @Column(name = "max_accept_value")
    public Long getMaxAcceptValue() {
        return maxAcceptValue;
    }

    public void setMaxAcceptValue(Long maxAcceptValue) {
        this.maxAcceptValue = maxAcceptValue;
    }

    @Basic
    @Column(name = "num_sys_reject_loan")
    public Integer getNumSysRejectLoan() {
        return numSysRejectLoan;
    }

    public void setNumSysRejectLoan(Integer numSysRejectLoan) {
        this.numSysRejectLoan = numSysRejectLoan;
    }

    @Basic
    @Column(name = "a3_rate")
    public Integer getA3Rate() {
        return a3Rate;
    }

    public void setA3Rate(Integer a3Rate) {
        this.a3Rate = a3Rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LenderProperties entity = (LenderProperties) o;
        return id == entity.id &&
                Objects.equals(a7Rate, entity.a7Rate) &&
                Objects.equals(numAcceptLoan, entity.numAcceptLoan) &&
                Objects.equals(numPushedLoan, entity.numPushedLoan) &&
                Objects.equals(acceptRate, entity.acceptRate) &&
                Objects.equals(maxAcceptValue, entity.maxAcceptValue) &&
                Objects.equals(numSysRejectLoan, entity.numSysRejectLoan) &&
                Objects.equals(a3Rate, entity.a3Rate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, a7Rate, numAcceptLoan, numPushedLoan, acceptRate, maxAcceptValue, numSysRejectLoan, a3Rate);
    }
}
