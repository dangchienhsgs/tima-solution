package com.tima.solution.entities.postgresql;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "system_feedback", schema = "public", catalog = "postgres")
public class SystemFeedbackEntity {
    private int id;
    private Timestamp createddate;
    private Integer priority;
    private Integer errorcode;
    private String msg;
    private Integer loanid;
    private Integer cityid;
    private Integer districtid;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "createddate")
    public Timestamp getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Timestamp createddate) {
        this.createddate = createddate;
    }

    @Basic
    @Column(name = "priority")
    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Basic
    @Column(name = "errorcode")
    public Integer getErrorcode() {
        return errorcode;
    }

    public void setErrorcode(Integer errorcode) {
        this.errorcode = errorcode;
    }

    @Basic
    @Column(name = "msg")
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Basic
    @Column(name = "loanid")
    public Integer getLoanid() {
        return loanid;
    }

    public void setLoanid(Integer loanid) {
        this.loanid = loanid;
    }

    @Basic
    @Column(name = "cityid")
    public Integer getCityid() {
        return cityid;
    }

    public void setCityid(Integer cityid) {
        this.cityid = cityid;
    }

    @Basic
    @Column(name = "districtid")
    public Integer getDistrictid() {
        return districtid;
    }

    public void setDistrictid(Integer districtid) {
        this.districtid = districtid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SystemFeedbackEntity entity = (SystemFeedbackEntity) o;
        return id == entity.id &&
                Objects.equals(createddate, entity.createddate) &&
                Objects.equals(priority, entity.priority) &&
                Objects.equals(errorcode, entity.errorcode) &&
                Objects.equals(msg, entity.msg) &&
                Objects.equals(loanid, entity.loanid) &&
                Objects.equals(cityid, entity.cityid) &&
                Objects.equals(districtid, entity.districtid);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, createddate, priority, errorcode, msg, loanid, cityid, districtid);
    }
}
