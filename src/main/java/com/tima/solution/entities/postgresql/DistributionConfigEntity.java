package com.tima.solution.entities.postgresql;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "distribution_config", schema = "public", catalog = "postgres")
public class DistributionConfigEntity {
    private int numLenderPerTime;
    private double distributionNewPercent;
    private Double distributionGoodAndActivePercent;
    private double distributionGoodAndInactivePercent;
    private double distributionBadAndActivePercent;
    private double distributionBadAndInactivePercent;
    private Integer lenderMinA3Active;
    private Integer lenderMinA7Active;
    private Integer id;

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "num_lender_per_time")
    public int getNumLenderPerTime() {
        return numLenderPerTime;
    }

    public void setNumLenderPerTime(int numLenderPerTime) {
        this.numLenderPerTime = numLenderPerTime;
    }

    @Basic
    @Column(name = "distribution_new_percent")
    public double getDistributionNewPercent() {
        return distributionNewPercent;
    }

    public void setDistributionNewPercent(double distributionNewPercent) {
        this.distributionNewPercent = distributionNewPercent;
    }

    @Basic
    @Column(name = "distribution_good_and_active_percent")
    public Double getDistributionGoodAndActivePercent() {
        return distributionGoodAndActivePercent;
    }

    public void setDistributionGoodAndActivePercent(Double distributionGoodAndActivePercent) {
        this.distributionGoodAndActivePercent = distributionGoodAndActivePercent;
    }

    @Basic
    @Column(name = "distribution_good_and_inactive_percent")
    public double getDistributionGoodAndInactivePercent() {
        return distributionGoodAndInactivePercent;
    }

    public void setDistributionGoodAndInactivePercent(double distributionGoodAndInactivePercent) {
        this.distributionGoodAndInactivePercent = distributionGoodAndInactivePercent;
    }

    @Basic
    @Column(name = "distribution_bad_and_active_percent")
    public double getDistributionBadAndActivePercent() {
        return distributionBadAndActivePercent;
    }

    public void setDistributionBadAndActivePercent(double distributionBadAndActivePercent) {
        this.distributionBadAndActivePercent = distributionBadAndActivePercent;
    }

    @Basic
    @Column(name = "distribution_bad_and_inactive_percent")
    public double getDistributionBadAndInactivePercent() {
        return distributionBadAndInactivePercent;
    }

    public void setDistributionBadAndInactivePercent(double distributionBadAndInactivePercent) {
        this.distributionBadAndInactivePercent = distributionBadAndInactivePercent;
    }

    @Basic
    @Column(name = "lender_min_a3_active")
    public Integer getLenderMinA3Active() {
        return lenderMinA3Active;
    }

    public void setLenderMinA3Active(Integer lenderMinA3Active) {
        this.lenderMinA3Active = lenderMinA3Active;
    }

    @Basic
    @Column(name = "lender_min_a7_active")
    public Integer getLenderMinA7Active() {
        return lenderMinA7Active;
    }

    public void setLenderMinA7Active(Integer lenderMinA7Active) {
        this.lenderMinA7Active = lenderMinA7Active;
    }

    @Id
    @Basic
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DistributionConfigEntity that = (DistributionConfigEntity) o;
        return numLenderPerTime == that.numLenderPerTime &&
                Double.compare(that.distributionNewPercent, distributionNewPercent) == 0 &&
                Double.compare(that.distributionGoodAndInactivePercent, distributionGoodAndInactivePercent) == 0 &&
                Double.compare(that.distributionBadAndActivePercent, distributionBadAndActivePercent) == 0 &&
                Double.compare(that.distributionBadAndInactivePercent, distributionBadAndInactivePercent) == 0 &&
                Objects.equals(distributionGoodAndActivePercent, that.distributionGoodAndActivePercent) &&
                Objects.equals(lenderMinA3Active, that.lenderMinA3Active) &&
                Objects.equals(lenderMinA7Active, that.lenderMinA7Active) &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(numLenderPerTime, distributionNewPercent, distributionGoodAndActivePercent, distributionGoodAndInactivePercent, distributionBadAndActivePercent, distributionBadAndInactivePercent, lenderMinA3Active, lenderMinA7Active, id);
    }
}
