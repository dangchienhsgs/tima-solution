package com.tima.solution.entities.mssql;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class TblPushProcessEntityPK implements Serializable {
    private int loanId;
    private int lenderId;

    public TblPushProcessEntityPK() {
    }

    public TblPushProcessEntityPK(int loanId, int lenderId) {
        this.loanId = loanId;
        this.lenderId = lenderId;
    }

    @Column(name = "LoanID")
    @Id
    public int getLoanId() {
        return loanId;
    }

    public void setLoanId(int loanId) {
        this.loanId = loanId;
    }

    @Column(name = "LenderID")
    @Id
    public int getLenderId() {
        return lenderId;
    }

    public void setLenderId(int lenderId) {
        this.lenderId = lenderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TblPushProcessEntityPK that = (TblPushProcessEntityPK) o;
        return loanId == that.loanId &&
                lenderId == that.lenderId;
    }

    @Override
    public int hashCode() {

        return Objects.hash(loanId, lenderId);
    }
}
