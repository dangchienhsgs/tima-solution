package com.tima.solution.entities.mssql;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "tblLoanCredit", schema = "dbo", catalog = "SanTima")
public class TblLoanCreditEntity {
    private int id;
    private Integer customerCreditId;
    private Long totalMoney;
    private Integer loanTime;
    private Short status;
    private Timestamp createDate;
    private Timestamp modifyDate;
    private Timestamp approveDate;
    private Date fromDate;
    private Date toDate;
    private Byte typeReceivingMoney;
    private String nameBanking;
    private String nameCardHolder;
    private String numberCardBanking;
    private String accountNumberBanking;
    private Byte typeId;
    private String utmSource;
    private String utmMedium;
    private String utmCampaign;
    private Byte typeTime;
    private int totalUserView;
    private Integer cityId;
    private Integer districtId;
    private Integer lenderAccept;
    private Timestamp nextProcess;
    private Integer isProcessPush;
    private Integer platformType;
    private Integer reasonId;
    private String logComment;
    private Integer owerLenderId;
    private Byte isPushTima;
    private Integer totalCallSupport;
    private Integer lockByUserId;
    private String lockByFullName;
    private String owerLenderName;
    private Long originalPrice;
    private Integer percentDiscount;
    private String linkAuthorization;
    private Long sellPrice;
    private Integer lenderId;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "CustomerCreditId")
    public Integer getCustomerCreditId() {
        return customerCreditId;
    }

    public void setCustomerCreditId(Integer customerCreditId) {
        this.customerCreditId = customerCreditId;
    }

    @Basic
    @Column(name = "TotalMoney")
    public Long getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Long totalMoney) {
        this.totalMoney = totalMoney;
    }

    @Basic
    @Column(name = "LoanTime")
    public Integer getLoanTime() {
        return loanTime;
    }

    public void setLoanTime(Integer loanTime) {
        this.loanTime = loanTime;
    }

    @Basic
    @Column(name = "Status")
    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    @Basic
    @Column(name = "CreateDate")
    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    @Basic
    @Column(name = "ModifyDate")
    public Timestamp getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Timestamp modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Basic
    @Column(name = "ApproveDate")
    public Timestamp getApproveDate() {
        return approveDate;
    }

    public void setApproveDate(Timestamp approveDate) {
        this.approveDate = approveDate;
    }

    @Basic
    @Column(name = "FromDate")
    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Basic
    @Column(name = "ToDate")
    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    @Basic
    @Column(name = "TypeReceivingMoney")
    public Byte getTypeReceivingMoney() {
        return typeReceivingMoney;
    }

    public void setTypeReceivingMoney(Byte typeReceivingMoney) {
        this.typeReceivingMoney = typeReceivingMoney;
    }

    @Basic
    @Column(name = "NameBanking")
    public String getNameBanking() {
        return nameBanking;
    }

    public void setNameBanking(String nameBanking) {
        this.nameBanking = nameBanking;
    }

    @Basic
    @Column(name = "NameCardHolder")
    public String getNameCardHolder() {
        return nameCardHolder;
    }

    public void setNameCardHolder(String nameCardHolder) {
        this.nameCardHolder = nameCardHolder;
    }

    @Basic
    @Column(name = "NumberCardBanking")
    public String getNumberCardBanking() {
        return numberCardBanking;
    }

    public void setNumberCardBanking(String numberCardBanking) {
        this.numberCardBanking = numberCardBanking;
    }

    @Basic
    @Column(name = "AccountNumberBanking")
    public String getAccountNumberBanking() {
        return accountNumberBanking;
    }

    public void setAccountNumberBanking(String accountNumberBanking) {
        this.accountNumberBanking = accountNumberBanking;
    }

    @Basic
    @Column(name = "TypeID")
    public Byte getTypeId() {
        return typeId;
    }

    public void setTypeId(Byte typeId) {
        this.typeId = typeId;
    }

    @Basic
    @Column(name = "utm_source")
    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }

    @Basic
    @Column(name = "utm_medium")
    public String getUtmMedium() {
        return utmMedium;
    }

    public void setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
    }

    @Basic
    @Column(name = "utm_campaign")
    public String getUtmCampaign() {
        return utmCampaign;
    }

    public void setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
    }

    @Basic
    @Column(name = "TypeTime")
    public Byte getTypeTime() {
        return typeTime;
    }

    public void setTypeTime(Byte typeTime) {
        this.typeTime = typeTime;
    }

    @Basic
    @Column(name = "TotalUserView")
    public int getTotalUserView() {
        return totalUserView;
    }

    public void setTotalUserView(int totalUserView) {
        this.totalUserView = totalUserView;
    }

    @Basic
    @Column(name = "CityId")
    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    @Basic
    @Column(name = "DistrictId")
    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    @Basic
    @Column(name = "LenderAccept")
    public Integer getLenderAccept() {
        return lenderAccept;
    }

    public void setLenderAccept(Integer lenderAccept) {
        this.lenderAccept = lenderAccept;
    }

    @Basic
    @Column(name = "NextProcess")
    public Timestamp getNextProcess() {
        return nextProcess;
    }

    public void setNextProcess(Timestamp nextProcess) {
        this.nextProcess = nextProcess;
    }

    @Basic
    @Column(name = "IsProcessPush")
    public Integer getIsProcessPush() {
        return isProcessPush;
    }

    public void setIsProcessPush(Integer isProcessPush) {
        this.isProcessPush = isProcessPush;
    }

    @Basic
    @Column(name = "PlatformType")
    public Integer getPlatformType() {
        return platformType;
    }

    public void setPlatformType(Integer platformType) {
        this.platformType = platformType;
    }

    @Basic
    @Column(name = "ReasonId")
    public Integer getReasonId() {
        return reasonId;
    }

    public void setReasonId(Integer reasonId) {
        this.reasonId = reasonId;
    }

    @Basic
    @Column(name = "LogComment")
    public String getLogComment() {
        return logComment;
    }

    public void setLogComment(String logComment) {
        this.logComment = logComment;
    }

    @Basic
    @Column(name = "OwerLenderID")
    public Integer getOwerLenderId() {
        return owerLenderId;
    }

    public void setOwerLenderId(Integer owerLenderId) {
        this.owerLenderId = owerLenderId;
    }

    @Basic
    @Column(name = "IsPushTima")
    public Byte getIsPushTima() {
        return isPushTima;
    }

    public void setIsPushTima(Byte isPushTima) {
        this.isPushTima = isPushTima;
    }

    @Basic
    @Column(name = "TotalCallSupport")
    public Integer getTotalCallSupport() {
        return totalCallSupport;
    }

    public void setTotalCallSupport(Integer totalCallSupport) {
        this.totalCallSupport = totalCallSupport;
    }

    @Basic
    @Column(name = "LockByUserId")
    public Integer getLockByUserId() {
        return lockByUserId;
    }

    public void setLockByUserId(Integer lockByUserId) {
        this.lockByUserId = lockByUserId;
    }

    @Basic
    @Column(name = "LockByFullName")
    public String getLockByFullName() {
        return lockByFullName;
    }

    public void setLockByFullName(String lockByFullName) {
        this.lockByFullName = lockByFullName;
    }

    @Basic
    @Column(name = "OwerLenderName")
    public String getOwerLenderName() {
        return owerLenderName;
    }

    public void setOwerLenderName(String owerLenderName) {
        this.owerLenderName = owerLenderName;
    }

    @Basic
    @Column(name = "OriginalPrice")
    public Long getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Long originalPrice) {
        this.originalPrice = originalPrice;
    }

    @Basic
    @Column(name = "PercentDiscount")
    public Integer getPercentDiscount() {
        return percentDiscount;
    }

    public void setPercentDiscount(Integer percentDiscount) {
        this.percentDiscount = percentDiscount;
    }

    @Basic
    @Column(name = "LinkAuthorization")
    public String getLinkAuthorization() {
        return linkAuthorization;
    }

    public void setLinkAuthorization(String linkAuthorization) {
        this.linkAuthorization = linkAuthorization;
    }

    @Basic
    @Column(name = "SellPrice")
    public Long getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Long sellPrice) {
        this.sellPrice = sellPrice;
    }

    @Basic
    @Column(name = "LenderId")
    public Integer getLenderId() {
        return lenderId;
    }

    public void setLenderId(Integer lenderId) {
        this.lenderId = lenderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TblLoanCreditEntity that = (TblLoanCreditEntity) o;
        return id == that.id &&
                totalUserView == that.totalUserView &&
                Objects.equals(customerCreditId, that.customerCreditId) &&
                Objects.equals(totalMoney, that.totalMoney) &&
                Objects.equals(loanTime, that.loanTime) &&
                Objects.equals(status, that.status) &&
                Objects.equals(createDate, that.createDate) &&
                Objects.equals(modifyDate, that.modifyDate) &&
                Objects.equals(approveDate, that.approveDate) &&
                Objects.equals(fromDate, that.fromDate) &&
                Objects.equals(toDate, that.toDate) &&
                Objects.equals(typeReceivingMoney, that.typeReceivingMoney) &&
                Objects.equals(nameBanking, that.nameBanking) &&
                Objects.equals(nameCardHolder, that.nameCardHolder) &&
                Objects.equals(numberCardBanking, that.numberCardBanking) &&
                Objects.equals(accountNumberBanking, that.accountNumberBanking) &&
                Objects.equals(typeId, that.typeId) &&
                Objects.equals(utmSource, that.utmSource) &&
                Objects.equals(utmMedium, that.utmMedium) &&
                Objects.equals(utmCampaign, that.utmCampaign) &&
                Objects.equals(typeTime, that.typeTime) &&
                Objects.equals(cityId, that.cityId) &&
                Objects.equals(districtId, that.districtId) &&
                Objects.equals(lenderAccept, that.lenderAccept) &&
                Objects.equals(nextProcess, that.nextProcess) &&
                Objects.equals(isProcessPush, that.isProcessPush) &&
                Objects.equals(platformType, that.platformType) &&
                Objects.equals(reasonId, that.reasonId) &&
                Objects.equals(logComment, that.logComment) &&
                Objects.equals(owerLenderId, that.owerLenderId) &&
                Objects.equals(isPushTima, that.isPushTima) &&
                Objects.equals(totalCallSupport, that.totalCallSupport) &&
                Objects.equals(lockByUserId, that.lockByUserId) &&
                Objects.equals(lockByFullName, that.lockByFullName) &&
                Objects.equals(owerLenderName, that.owerLenderName) &&
                Objects.equals(originalPrice, that.originalPrice) &&
                Objects.equals(percentDiscount, that.percentDiscount) &&
                Objects.equals(linkAuthorization, that.linkAuthorization) &&
                Objects.equals(sellPrice, that.sellPrice) &&
                Objects.equals(lenderId, that.lenderId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, customerCreditId, totalMoney, loanTime, status, createDate, modifyDate, approveDate, fromDate, toDate, typeReceivingMoney, nameBanking, nameCardHolder, numberCardBanking, accountNumberBanking, typeId, utmSource, utmMedium, utmCampaign, typeTime, totalUserView, cityId, districtId, lenderAccept, nextProcess, isProcessPush, platformType, reasonId, logComment, owerLenderId, isPushTima, totalCallSupport, lockByUserId, lockByFullName, owerLenderName, originalPrice, percentDiscount, linkAuthorization, sellPrice, lenderId);
    }
}
