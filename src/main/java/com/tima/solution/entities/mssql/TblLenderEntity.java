package com.tima.solution.entities.mssql;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "tblLender", schema = "dbo", catalog = "SanTima")
public class TblLenderEntity {
    private int id;
    private String userName;
    private String password;
    private Integer status;
    private String tokenKey;
    private Timestamp updatedDate;
    private String fullName;
    private Integer cityId;
    private Integer districtId;
    private Integer wardId;
    private String address;
    private Integer lenderId;
    private Integer systemId;
    private String phone;
    private Timestamp createDate;
    private Short gender;
    private String avatar;
    private Float vote;
    private String otpCode;
    private Long loanAccept;
    private Long loanCancel;
    private Long loanSystemCancel;
    private Long totalLoan;
    private Integer loanPushToday;
    private Short typeLenderRegister;
    private Byte isLock;
    private Timestamp lastLogin;
    private Integer lastLoginPlatform;
    private Integer vip;
    private Date fromDate;
    private Date toDate;
    private Integer maxLoanADay;
    private Integer totalLoanBuy;
    private Integer totalLoanPush;
    private Integer reasonId;
    private String logComment;
    private Integer platformType;
    private Long money;
    private Timestamp lastDateCallSupport;
    private String note;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "UserName")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "Password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "Status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "TokenKey")
    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

    @Basic
    @Column(name = "UpdatedDate")
    public Timestamp getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Timestamp updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Basic
    @Column(name = "FullName")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Basic
    @Column(name = "CityId")
    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    @Basic
    @Column(name = "DistrictId")
    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    @Basic
    @Column(name = "WardId")
    public Integer getWardId() {
        return wardId;
    }

    public void setWardId(Integer wardId) {
        this.wardId = wardId;
    }

    @Basic
    @Column(name = "Address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "LenderId")
    public Integer getLenderId() {
        return lenderId;
    }

    public void setLenderId(Integer lenderId) {
        this.lenderId = lenderId;
    }

    @Basic
    @Column(name = "SystemId")
    public Integer getSystemId() {
        return systemId;
    }

    public void setSystemId(Integer systemId) {
        this.systemId = systemId;
    }

    @Basic
    @Column(name = "Phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "CreateDate")
    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    @Basic
    @Column(name = "Gender")
    public Short getGender() {
        return gender;
    }

    public void setGender(Short gender) {
        this.gender = gender;
    }

    @Basic
    @Column(name = "Avatar")
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Basic
    @Column(name = "Vote")
    public Float getVote() {
        return vote;
    }

    public void setVote(Float vote) {
        this.vote = vote;
    }

    @Basic
    @Column(name = "OTPCode")
    public String getOtpCode() {
        return otpCode;
    }

    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }

    @Basic
    @Column(name = "LoanAccept")
    public Long getLoanAccept() {
        return loanAccept;
    }

    public void setLoanAccept(Long loanAccept) {
        this.loanAccept = loanAccept;
    }

    @Basic
    @Column(name = "LoanCancel")
    public Long getLoanCancel() {
        return loanCancel;
    }

    public void setLoanCancel(Long loanCancel) {
        this.loanCancel = loanCancel;
    }

    @Basic
    @Column(name = "LoanSystemCancel")
    public Long getLoanSystemCancel() {
        return loanSystemCancel;
    }

    public void setLoanSystemCancel(Long loanSystemCancel) {
        this.loanSystemCancel = loanSystemCancel;
    }

    @Basic
    @Column(name = "TotalLoan")
    public Long getTotalLoan() {
        return totalLoan;
    }

    public void setTotalLoan(Long totalLoan) {
        this.totalLoan = totalLoan;
    }

    @Basic
    @Column(name = "LoanPushToday")
    public Integer getLoanPushToday() {
        return loanPushToday;
    }

    public void setLoanPushToday(Integer loanPushToday) {
        this.loanPushToday = loanPushToday;
    }

    @Basic
    @Column(name = "TypeLenderRegister")
    public Short getTypeLenderRegister() {
        return typeLenderRegister;
    }

    public void setTypeLenderRegister(Short typeLenderRegister) {
        this.typeLenderRegister = typeLenderRegister;
    }

    @Basic
    @Column(name = "IsLock")
    public Byte getIsLock() {
        return isLock;
    }

    public void setIsLock(Byte isLock) {
        this.isLock = isLock;
    }

    @Basic
    @Column(name = "LastLogin")
    public Timestamp getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Timestamp lastLogin) {
        this.lastLogin = lastLogin;
    }

    @Basic
    @Column(name = "LastLoginPlatform")
    public Integer getLastLoginPlatform() {
        return lastLoginPlatform;
    }

    public void setLastLoginPlatform(Integer lastLoginPlatform) {
        this.lastLoginPlatform = lastLoginPlatform;
    }

    @Basic
    @Column(name = "Vip")
    public Integer getVip() {
        return vip;
    }

    public void setVip(Integer vip) {
        this.vip = vip;
    }

    @Basic
    @Column(name = "FromDate")
    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Basic
    @Column(name = "ToDate")
    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    @Basic
    @Column(name = "MaxLoanADay")
    public Integer getMaxLoanADay() {
        return maxLoanADay;
    }

    public void setMaxLoanADay(Integer maxLoanADay) {
        this.maxLoanADay = maxLoanADay;
    }

    @Basic
    @Column(name = "TotalLoanBuy")
    public Integer getTotalLoanBuy() {
        return totalLoanBuy;
    }

    public void setTotalLoanBuy(Integer totalLoanBuy) {
        this.totalLoanBuy = totalLoanBuy;
    }

    @Basic
    @Column(name = "TotalLoanPush")
    public Integer getTotalLoanPush() {
        return totalLoanPush;
    }

    public void setTotalLoanPush(Integer totalLoanPush) {
        this.totalLoanPush = totalLoanPush;
    }

    @Basic
    @Column(name = "ReasonId")
    public Integer getReasonId() {
        return reasonId;
    }

    public void setReasonId(Integer reasonId) {
        this.reasonId = reasonId;
    }

    @Basic
    @Column(name = "LogComment")
    public String getLogComment() {
        return logComment;
    }

    public void setLogComment(String logComment) {
        this.logComment = logComment;
    }

    @Basic
    @Column(name = "PlatformType")
    public Integer getPlatformType() {
        return platformType;
    }

    public void setPlatformType(Integer platformType) {
        this.platformType = platformType;
    }

    @Basic
    @Column(name = "Money")
    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    @Basic
    @Column(name = "LastDateCallSupport")
    public Timestamp getLastDateCallSupport() {
        return lastDateCallSupport;
    }

    public void setLastDateCallSupport(Timestamp lastDateCallSupport) {
        this.lastDateCallSupport = lastDateCallSupport;
    }

    @Basic
    @Column(name = "Note")
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TblLenderEntity that = (TblLenderEntity) o;
        return id == that.id &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(password, that.password) &&
                Objects.equals(status, that.status) &&
                Objects.equals(tokenKey, that.tokenKey) &&
                Objects.equals(updatedDate, that.updatedDate) &&
                Objects.equals(fullName, that.fullName) &&
                Objects.equals(cityId, that.cityId) &&
                Objects.equals(districtId, that.districtId) &&
                Objects.equals(wardId, that.wardId) &&
                Objects.equals(address, that.address) &&
                Objects.equals(lenderId, that.lenderId) &&
                Objects.equals(systemId, that.systemId) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(createDate, that.createDate) &&
                Objects.equals(gender, that.gender) &&
                Objects.equals(avatar, that.avatar) &&
                Objects.equals(vote, that.vote) &&
                Objects.equals(otpCode, that.otpCode) &&
                Objects.equals(loanAccept, that.loanAccept) &&
                Objects.equals(loanCancel, that.loanCancel) &&
                Objects.equals(loanSystemCancel, that.loanSystemCancel) &&
                Objects.equals(totalLoan, that.totalLoan) &&
                Objects.equals(loanPushToday, that.loanPushToday) &&
                Objects.equals(typeLenderRegister, that.typeLenderRegister) &&
                Objects.equals(isLock, that.isLock) &&
                Objects.equals(lastLogin, that.lastLogin) &&
                Objects.equals(lastLoginPlatform, that.lastLoginPlatform) &&
                Objects.equals(vip, that.vip) &&
                Objects.equals(fromDate, that.fromDate) &&
                Objects.equals(toDate, that.toDate) &&
                Objects.equals(maxLoanADay, that.maxLoanADay) &&
                Objects.equals(totalLoanBuy, that.totalLoanBuy) &&
                Objects.equals(totalLoanPush, that.totalLoanPush) &&
                Objects.equals(reasonId, that.reasonId) &&
                Objects.equals(logComment, that.logComment) &&
                Objects.equals(platformType, that.platformType) &&
                Objects.equals(money, that.money) &&
                Objects.equals(lastDateCallSupport, that.lastDateCallSupport) &&
                Objects.equals(note, that.note);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, userName, password, status, tokenKey, updatedDate, fullName, cityId, districtId, wardId, address, lenderId, systemId, phone, createDate, gender, avatar, vote, otpCode, loanAccept, loanCancel, loanSystemCancel, totalLoan, loanPushToday, typeLenderRegister, isLock, lastLogin, lastLoginPlatform, vip, fromDate, toDate, maxLoanADay, totalLoanBuy, totalLoanPush, reasonId, logComment, platformType, money, lastDateCallSupport, note);
    }
}
