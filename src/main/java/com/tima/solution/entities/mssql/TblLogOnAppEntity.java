package com.tima.solution.entities.mssql;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "tblLogOnApp", schema = "dbo", catalog = "SanTima")
public class TblLogOnAppEntity {
    private long id;
    private Integer userId;
    private Integer userType;
    private String fcmKey;
    private String ipAddress;
    private String lat;
    private String lng;
    private Timestamp createDate;
    private Integer platform;
    private Integer loginType;

    @Id
    @Column(name = "Id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "UserId")
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "UserType")
    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    @Basic
    @Column(name = "FcmKey")
    public String getFcmKey() {
        return fcmKey;
    }

    public void setFcmKey(String fcmKey) {
        this.fcmKey = fcmKey;
    }

    @Basic
    @Column(name = "IpAddress")
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @Basic
    @Column(name = "Lat")
    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    @Basic
    @Column(name = "Lng")
    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    @Basic
    @Column(name = "CreateDate")
    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    @Basic
    @Column(name = "Platform")
    public Integer getPlatform() {
        return platform;
    }

    public void setPlatform(Integer platform) {
        this.platform = platform;
    }

    @Basic
    @Column(name = "LoginType")
    public Integer getLoginType() {
        return loginType;
    }

    public void setLoginType(Integer loginType) {
        this.loginType = loginType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TblLogOnAppEntity that = (TblLogOnAppEntity) o;
        return id == that.id &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(userType, that.userType) &&
                Objects.equals(fcmKey, that.fcmKey) &&
                Objects.equals(ipAddress, that.ipAddress) &&
                Objects.equals(lat, that.lat) &&
                Objects.equals(lng, that.lng) &&
                Objects.equals(createDate, that.createDate) &&
                Objects.equals(platform, that.platform) &&
                Objects.equals(loginType, that.loginType);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, userId, userType, fcmKey, ipAddress, lat, lng, createDate, platform, loginType);
    }
}
