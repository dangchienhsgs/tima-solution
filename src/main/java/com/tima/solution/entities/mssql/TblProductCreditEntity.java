package com.tima.solution.entities.mssql;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tblProductCredit", schema = "dbo", catalog = "SanTima")
public class TblProductCreditEntity {
    private int id;
    private String name;
    private Short status;
    private Byte typeId;
    private String nameType;
    private Integer position;
    private Integer typeView;
    private String urlIcon;
    private String urlAddress;
    private Long price;

    @Id
    @Column(name = "Id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "Status")
    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    @Basic
    @Column(name = "TypeId")
    public Byte getTypeId() {
        return typeId;
    }

    public void setTypeId(Byte typeId) {
        this.typeId = typeId;
    }

    @Basic
    @Column(name = "NameType")
    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    @Basic
    @Column(name = "Position")
    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    @Basic
    @Column(name = "TypeView")
    public Integer getTypeView() {
        return typeView;
    }

    public void setTypeView(Integer typeView) {
        this.typeView = typeView;
    }

    @Basic
    @Column(name = "UrlIcon")
    public String getUrlIcon() {
        return urlIcon;
    }

    public void setUrlIcon(String urlIcon) {
        this.urlIcon = urlIcon;
    }

    @Basic
    @Column(name = "UrlAddress")
    public String getUrlAddress() {
        return urlAddress;
    }

    public void setUrlAddress(String urlAddress) {
        this.urlAddress = urlAddress;
    }

    @Basic
    @Column(name = "Price")
    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TblProductCreditEntity that = (TblProductCreditEntity) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(status, that.status) &&
                Objects.equals(typeId, that.typeId) &&
                Objects.equals(nameType, that.nameType) &&
                Objects.equals(position, that.position) &&
                Objects.equals(typeView, that.typeView) &&
                Objects.equals(urlIcon, that.urlIcon) &&
                Objects.equals(urlAddress, that.urlAddress) &&
                Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, status, typeId, nameType, position, typeView, urlIcon, urlAddress, price);
    }
}
