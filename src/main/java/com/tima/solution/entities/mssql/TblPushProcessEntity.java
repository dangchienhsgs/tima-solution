package com.tima.solution.entities.mssql;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "tblPushProcess", schema = "dbo", catalog = "SanTima")
@IdClass(TblPushProcessEntityPK.class)
public class TblPushProcessEntity {
    private int loanId;
    private int lenderId;
    private Timestamp pushTime;
    private String fcmKey;
    private Byte isSend;
    private Float totalScore;
    private String phoneLender;
    private Long totalMoney;
    private Integer loanTime;
    private Integer customerCreditId;
    private Float fcmKeyPoint;
    private Float loanAcceptPoint;
    private Float loanCancelPoint;
    private Float loanPushTodayPoint;
    private Float distancePoint;
    private Integer vip;

    @Id
    @Column(name = "LoanID")
    public int getLoanId() {
        return loanId;
    }

    public void setLoanId(int loanId) {
        this.loanId = loanId;
    }

    @Id
    @Column(name = "LenderID")
    public int getLenderId() {
        return lenderId;
    }

    public void setLenderId(int lenderId) {
        this.lenderId = lenderId;
    }

    @Basic
    @Column(name = "PushTime")
    public Timestamp getPushTime() {
        return pushTime;
    }

    public void setPushTime(Timestamp pushTime) {
        this.pushTime = pushTime;
    }

    @Basic
    @Column(name = "FcmKey")
    public String getFcmKey() {
        return fcmKey;
    }

    public void setFcmKey(String fcmKey) {
        this.fcmKey = fcmKey;
    }

    @Basic
    @Column(name = "IsSend")
    public Byte getIsSend() {
        return isSend;
    }

    public void setIsSend(Byte isSend) {
        this.isSend = isSend;
    }

    @Basic
    @Column(name = "TotalScore")
    public Float getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Float totalScore) {
        this.totalScore = totalScore;
    }

    @Basic
    @Column(name = "PhoneLender")
    public String getPhoneLender() {
        return phoneLender;
    }

    public void setPhoneLender(String phoneLender) {
        this.phoneLender = phoneLender;
    }

    @Basic
    @Column(name = "TotalMoney")
    public Long getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Long totalMoney) {
        this.totalMoney = totalMoney;
    }

    @Basic
    @Column(name = "LoanTime")
    public Integer getLoanTime() {
        return loanTime;
    }

    public void setLoanTime(Integer loanTime) {
        this.loanTime = loanTime;
    }

    @Basic
    @Column(name = "CustomerCreditId")
    public Integer getCustomerCreditId() {
        return customerCreditId;
    }

    public void setCustomerCreditId(Integer customerCreditId) {
        this.customerCreditId = customerCreditId;
    }

    @Basic
    @Column(name = "FcmKeyPoint")
    public Float getFcmKeyPoint() {
        return fcmKeyPoint;
    }

    public void setFcmKeyPoint(Float fcmKeyPoint) {
        this.fcmKeyPoint = fcmKeyPoint;
    }

    @Basic
    @Column(name = "LoanAcceptPoint")
    public Float getLoanAcceptPoint() {
        return loanAcceptPoint;
    }

    public void setLoanAcceptPoint(Float loanAcceptPoint) {
        this.loanAcceptPoint = loanAcceptPoint;
    }

    @Basic
    @Column(name = "LoanCancelPoint")
    public Float getLoanCancelPoint() {
        return loanCancelPoint;
    }

    public void setLoanCancelPoint(Float loanCancelPoint) {
        this.loanCancelPoint = loanCancelPoint;
    }

    @Basic
    @Column(name = "LoanPushTodayPoint")
    public Float getLoanPushTodayPoint() {
        return loanPushTodayPoint;
    }

    public void setLoanPushTodayPoint(Float loanPushTodayPoint) {
        this.loanPushTodayPoint = loanPushTodayPoint;
    }

    @Basic
    @Column(name = "DistancePoint")
    public Float getDistancePoint() {
        return distancePoint;
    }

    public void setDistancePoint(Float distancePoint) {
        this.distancePoint = distancePoint;
    }

    @Basic
    @Column(name = "Vip")
    public Integer getVip() {
        return vip;
    }

    public void setVip(Integer vip) {
        this.vip = vip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TblPushProcessEntity that = (TblPushProcessEntity) o;
        return loanId == that.loanId &&
                lenderId == that.lenderId &&
                Objects.equals(pushTime, that.pushTime) &&
                Objects.equals(fcmKey, that.fcmKey) &&
                Objects.equals(isSend, that.isSend) &&
                Objects.equals(totalScore, that.totalScore) &&
                Objects.equals(phoneLender, that.phoneLender) &&
                Objects.equals(totalMoney, that.totalMoney) &&
                Objects.equals(loanTime, that.loanTime) &&
                Objects.equals(customerCreditId, that.customerCreditId) &&
                Objects.equals(fcmKeyPoint, that.fcmKeyPoint) &&
                Objects.equals(loanAcceptPoint, that.loanAcceptPoint) &&
                Objects.equals(loanCancelPoint, that.loanCancelPoint) &&
                Objects.equals(loanPushTodayPoint, that.loanPushTodayPoint) &&
                Objects.equals(distancePoint, that.distancePoint) &&
                Objects.equals(vip, that.vip);
    }

    @Override
    public int hashCode() {

        return Objects.hash(loanId, lenderId, pushTime, fcmKey, isSend, totalScore, phoneLender, totalMoney, loanTime, customerCreditId, fcmKeyPoint, loanAcceptPoint, loanCancelPoint, loanPushTodayPoint, distancePoint, vip);
    }
}
