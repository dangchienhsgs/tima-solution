package com.tima.solution.entities.mssql;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "tblCustomerCredit", schema = "dbo", catalog = "SanTima")
public class TblCustomerCreditEntity {
    private int id;
    private String userName;
    private String password;
    private String fullName;
    private Date birthday;
    private String phone;
    private String cardNumber;
    private Integer cityId;
    private Integer districtId;
    private Integer wardId;
    private Short gender;
    private String email;
    private String street;
    private String numberLiving;
    private String livingApartmentNumber;
    private Byte livingTimeId;
    private Byte typeOfOwnershipId;
    private Byte isResidential;
    private Byte jobId;
    private Byte workingIndustryId;
    private String companyName;
    private String companyPhone;
    private Long salary;
    private Byte receiveYourIncome;
    private Byte relativeFamilyId;
    private String fullNameFamily;
    private String phoneFamily;
    private Byte manufacturerId;
    private String modelPhone;
    private String yearMade;
    private Timestamp createDate;
    private String facebook;
    private String addressCompany;
    private Integer status;
    private String tokenKey;
    private Timestamp updatedDate;
    private String avatar;
    private Float vote;
    private String otpCode;
    private String contactNameCompany;
    private String companyTaxCode;
    private String descriptionPositionJob;
    private Integer companyDistrictId;
    private Integer companyCityId;
    private Integer platformType;
    private Integer isVerifyInfo;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "UserName")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "Password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "FullName")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Basic
    @Column(name = "Birthday")
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Basic
    @Column(name = "Phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "CardNumber")
    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Basic
    @Column(name = "CityId")
    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    @Basic
    @Column(name = "DistrictId")
    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    @Basic
    @Column(name = "WardId")
    public Integer getWardId() {
        return wardId;
    }

    public void setWardId(Integer wardId) {
        this.wardId = wardId;
    }

    @Basic
    @Column(name = "Gender")
    public Short getGender() {
        return gender;
    }

    public void setGender(Short gender) {
        this.gender = gender;
    }

    @Basic
    @Column(name = "Email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "Street")
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Basic
    @Column(name = "NumberLiving")
    public String getNumberLiving() {
        return numberLiving;
    }

    public void setNumberLiving(String numberLiving) {
        this.numberLiving = numberLiving;
    }

    @Basic
    @Column(name = "LivingApartmentNumber")
    public String getLivingApartmentNumber() {
        return livingApartmentNumber;
    }

    public void setLivingApartmentNumber(String livingApartmentNumber) {
        this.livingApartmentNumber = livingApartmentNumber;
    }

    @Basic
    @Column(name = "LivingTimeId")
    public Byte getLivingTimeId() {
        return livingTimeId;
    }

    public void setLivingTimeId(Byte livingTimeId) {
        this.livingTimeId = livingTimeId;
    }

    @Basic
    @Column(name = "TypeOfOwnershipId")
    public Byte getTypeOfOwnershipId() {
        return typeOfOwnershipId;
    }

    public void setTypeOfOwnershipId(Byte typeOfOwnershipId) {
        this.typeOfOwnershipId = typeOfOwnershipId;
    }

    @Basic
    @Column(name = "IsResidential")
    public Byte getIsResidential() {
        return isResidential;
    }

    public void setIsResidential(Byte isResidential) {
        this.isResidential = isResidential;
    }

    @Basic
    @Column(name = "JobId")
    public Byte getJobId() {
        return jobId;
    }

    public void setJobId(Byte jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "WorkingIndustryId")
    public Byte getWorkingIndustryId() {
        return workingIndustryId;
    }

    public void setWorkingIndustryId(Byte workingIndustryId) {
        this.workingIndustryId = workingIndustryId;
    }

    @Basic
    @Column(name = "CompanyName")
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Basic
    @Column(name = "CompanyPhone")
    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    @Basic
    @Column(name = "Salary")
    public Long getSalary() {
        return salary;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }

    @Basic
    @Column(name = "ReceiveYourIncome")
    public Byte getReceiveYourIncome() {
        return receiveYourIncome;
    }

    public void setReceiveYourIncome(Byte receiveYourIncome) {
        this.receiveYourIncome = receiveYourIncome;
    }

    @Basic
    @Column(name = "RelativeFamilyId")
    public Byte getRelativeFamilyId() {
        return relativeFamilyId;
    }

    public void setRelativeFamilyId(Byte relativeFamilyId) {
        this.relativeFamilyId = relativeFamilyId;
    }

    @Basic
    @Column(name = "FullNameFamily")
    public String getFullNameFamily() {
        return fullNameFamily;
    }

    public void setFullNameFamily(String fullNameFamily) {
        this.fullNameFamily = fullNameFamily;
    }

    @Basic
    @Column(name = "PhoneFamily")
    public String getPhoneFamily() {
        return phoneFamily;
    }

    public void setPhoneFamily(String phoneFamily) {
        this.phoneFamily = phoneFamily;
    }

    @Basic
    @Column(name = "ManufacturerId")
    public Byte getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Byte manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    @Basic
    @Column(name = "ModelPhone")
    public String getModelPhone() {
        return modelPhone;
    }

    public void setModelPhone(String modelPhone) {
        this.modelPhone = modelPhone;
    }

    @Basic
    @Column(name = "YearMade")
    public String getYearMade() {
        return yearMade;
    }

    public void setYearMade(String yearMade) {
        this.yearMade = yearMade;
    }

    @Basic
    @Column(name = "CreateDate")
    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    @Basic
    @Column(name = "Facebook")
    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    @Basic
    @Column(name = "AddressCompany")
    public String getAddressCompany() {
        return addressCompany;
    }

    public void setAddressCompany(String addressCompany) {
        this.addressCompany = addressCompany;
    }

    @Basic
    @Column(name = "Status")
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "TokenKey")
    public String getTokenKey() {
        return tokenKey;
    }

    public void setTokenKey(String tokenKey) {
        this.tokenKey = tokenKey;
    }

    @Basic
    @Column(name = "UpdatedDate")
    public Timestamp getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Timestamp updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Basic
    @Column(name = "Avatar")
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Basic
    @Column(name = "Vote")
    public Float getVote() {
        return vote;
    }

    public void setVote(Float vote) {
        this.vote = vote;
    }

    @Basic
    @Column(name = "OTPCode")
    public String getOtpCode() {
        return otpCode;
    }

    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }

    @Basic
    @Column(name = "ContactNameCompany")
    public String getContactNameCompany() {
        return contactNameCompany;
    }

    public void setContactNameCompany(String contactNameCompany) {
        this.contactNameCompany = contactNameCompany;
    }

    @Basic
    @Column(name = "CompanyTaxCode")
    public String getCompanyTaxCode() {
        return companyTaxCode;
    }

    public void setCompanyTaxCode(String companyTaxCode) {
        this.companyTaxCode = companyTaxCode;
    }

    @Basic
    @Column(name = "DescriptionPositionJob")
    public String getDescriptionPositionJob() {
        return descriptionPositionJob;
    }

    public void setDescriptionPositionJob(String descriptionPositionJob) {
        this.descriptionPositionJob = descriptionPositionJob;
    }

    @Basic
    @Column(name = "CompanyDistrictId")
    public Integer getCompanyDistrictId() {
        return companyDistrictId;
    }

    public void setCompanyDistrictId(Integer companyDistrictId) {
        this.companyDistrictId = companyDistrictId;
    }

    @Basic
    @Column(name = "CompanyCityId")
    public Integer getCompanyCityId() {
        return companyCityId;
    }

    public void setCompanyCityId(Integer companyCityId) {
        this.companyCityId = companyCityId;
    }

    @Basic
    @Column(name = "PlatformType")
    public Integer getPlatformType() {
        return platformType;
    }

    public void setPlatformType(Integer platformType) {
        this.platformType = platformType;
    }

    @Basic
    @Column(name = "IsVerifyInfo")
    public Integer getIsVerifyInfo() {
        return isVerifyInfo;
    }

    public void setIsVerifyInfo(Integer isVerifyInfo) {
        this.isVerifyInfo = isVerifyInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TblCustomerCreditEntity that = (TblCustomerCreditEntity) o;
        return id == that.id &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(password, that.password) &&
                Objects.equals(fullName, that.fullName) &&
                Objects.equals(birthday, that.birthday) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(cardNumber, that.cardNumber) &&
                Objects.equals(cityId, that.cityId) &&
                Objects.equals(districtId, that.districtId) &&
                Objects.equals(wardId, that.wardId) &&
                Objects.equals(gender, that.gender) &&
                Objects.equals(email, that.email) &&
                Objects.equals(street, that.street) &&
                Objects.equals(numberLiving, that.numberLiving) &&
                Objects.equals(livingApartmentNumber, that.livingApartmentNumber) &&
                Objects.equals(livingTimeId, that.livingTimeId) &&
                Objects.equals(typeOfOwnershipId, that.typeOfOwnershipId) &&
                Objects.equals(isResidential, that.isResidential) &&
                Objects.equals(jobId, that.jobId) &&
                Objects.equals(workingIndustryId, that.workingIndustryId) &&
                Objects.equals(companyName, that.companyName) &&
                Objects.equals(companyPhone, that.companyPhone) &&
                Objects.equals(salary, that.salary) &&
                Objects.equals(receiveYourIncome, that.receiveYourIncome) &&
                Objects.equals(relativeFamilyId, that.relativeFamilyId) &&
                Objects.equals(fullNameFamily, that.fullNameFamily) &&
                Objects.equals(phoneFamily, that.phoneFamily) &&
                Objects.equals(manufacturerId, that.manufacturerId) &&
                Objects.equals(modelPhone, that.modelPhone) &&
                Objects.equals(yearMade, that.yearMade) &&
                Objects.equals(createDate, that.createDate) &&
                Objects.equals(facebook, that.facebook) &&
                Objects.equals(addressCompany, that.addressCompany) &&
                Objects.equals(status, that.status) &&
                Objects.equals(tokenKey, that.tokenKey) &&
                Objects.equals(updatedDate, that.updatedDate) &&
                Objects.equals(avatar, that.avatar) &&
                Objects.equals(vote, that.vote) &&
                Objects.equals(otpCode, that.otpCode) &&
                Objects.equals(contactNameCompany, that.contactNameCompany) &&
                Objects.equals(companyTaxCode, that.companyTaxCode) &&
                Objects.equals(descriptionPositionJob, that.descriptionPositionJob) &&
                Objects.equals(companyDistrictId, that.companyDistrictId) &&
                Objects.equals(companyCityId, that.companyCityId) &&
                Objects.equals(platformType, that.platformType) &&
                Objects.equals(isVerifyInfo, that.isVerifyInfo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, userName, password, fullName, birthday, phone, cardNumber, cityId, districtId, wardId, gender, email, street, numberLiving, livingApartmentNumber, livingTimeId, typeOfOwnershipId, isResidential, jobId, workingIndustryId, companyName, companyPhone, salary, receiveYourIncome, relativeFamilyId, fullNameFamily, phoneFamily, manufacturerId, modelPhone, yearMade, createDate, facebook, addressCompany, status, tokenKey, updatedDate, avatar, vote, otpCode, contactNameCompany, companyTaxCode, descriptionPositionJob, companyDistrictId, companyCityId, platformType, isVerifyInfo);
    }
}
