package com.tima.solution.repositories.postgres;

import com.tima.solution.entities.postgresql.DistributionConfigEntity;
import org.springframework.data.repository.CrudRepository;

public interface DistributionConfigRepository extends CrudRepository<DistributionConfigEntity, Integer> {
}
