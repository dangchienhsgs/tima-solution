package com.tima.solution.repositories.postgres;

import com.tima.solution.entities.postgresql.SystemFeedbackEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SystemFeedbackRepository extends CrudRepository<SystemFeedbackEntity, Integer> {

    List<SystemFeedbackEntity> findAllByCityidOrderByCreateddate(int city);

    List<SystemFeedbackEntity> findAllByDistrictidOrderByCreateddate(int district);

}
