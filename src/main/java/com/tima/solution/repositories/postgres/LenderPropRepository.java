package com.tima.solution.repositories.postgres;

import com.tima.solution.entities.postgresql.LenderProperties;
import org.springframework.data.repository.CrudRepository;

public interface LenderPropRepository extends CrudRepository<LenderProperties, Integer> {

}
