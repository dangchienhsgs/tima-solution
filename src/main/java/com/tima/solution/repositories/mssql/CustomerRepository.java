package com.tima.solution.repositories.mssql;

import com.tima.solution.entities.mssql.TblCustomerCreditEntity;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<TblCustomerCreditEntity, Integer> {

}
