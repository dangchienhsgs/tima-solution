package com.tima.solution.repositories.mssql;

import com.tima.solution.entities.mssql.TblLenderEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LenderRepository extends CrudRepository<TblLenderEntity, Integer> {
    List<TblLenderEntity> findByIdIn(List<Integer> ids);
}
