package com.tima.solution.repositories.mssql;

import com.tima.solution.entities.mssql.TblPushProcessEntity;
import com.tima.solution.entities.mssql.TblPushProcessEntityPK;
import org.springframework.data.repository.CrudRepository;

public interface TblPushProcessRepository extends CrudRepository<TblPushProcessEntity, TblPushProcessEntityPK> {
    Iterable<TblPushProcessEntity> findAllByLoanId(int loan);
}
