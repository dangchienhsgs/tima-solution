package com.tima.solution.repositories.mssql;

import com.tima.solution.entities.mssql.TblManageSpicesEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ManageSpiceRepository extends CrudRepository<TblManageSpicesEntity, Integer> {

    Iterable<TblManageSpicesEntity> findAllByLenderId(int lender);

    List<TblManageSpicesEntity> findAllByLenderIdAndProductId(int lenderId, int productId);
}
