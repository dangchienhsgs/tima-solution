package com.tima.solution.repositories.mssql;

import com.tima.solution.entities.mssql.TblLogOnAppEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface LogOnAppRepository extends CrudRepository<TblLogOnAppEntity, Integer> {

    @Query(value = "select * from tblLogOnApp where CreateDate > :from and CreateDate < :to", nativeQuery = true)
    Iterable<TblLogOnAppEntity> getLogOnApp(@Param("from") Date from, @Param("to") Date to);
}
