package com.tima.solution.repositories.mssql;

import com.tima.solution.entities.mssql.TblLoanCreditEntity;
import org.springframework.data.repository.CrudRepository;

public interface LoanRepository extends CrudRepository<TblLoanCreditEntity, Integer> {

}
