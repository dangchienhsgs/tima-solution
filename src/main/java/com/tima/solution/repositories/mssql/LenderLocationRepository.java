package com.tima.solution.repositories.mssql;

import com.tima.solution.entities.mssql.TblLenderLocationEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LenderLocationRepository extends CrudRepository<TblLenderLocationEntity, Integer> {

    List<TblLenderLocationEntity> findAllByCityIdAndDistrictId(int cityId, int districtId);
}
