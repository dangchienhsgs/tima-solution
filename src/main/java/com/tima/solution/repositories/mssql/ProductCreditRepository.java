package com.tima.solution.repositories.mssql;

import com.tima.solution.entities.mssql.TblProductCreditEntity;
import org.springframework.data.repository.CrudRepository;

public interface ProductCreditRepository extends CrudRepository<TblProductCreditEntity, Integer> {
}
