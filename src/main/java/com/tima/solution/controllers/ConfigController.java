package com.tima.solution.controllers;

import com.tima.solution.configs.DistributionConfig;
import com.tima.solution.entities.postgresql.DistributionConfigEntity;
import com.tima.solution.model.response.ReloadConfigResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigController {

    private DistributionConfig config;

    @RequestMapping("/config/all")
    public DistributionConfigEntity getConfig() {
        return config.getConfig();
    }

    @Autowired
    public void setConfig(DistributionConfig config) {
        this.config = config;
    }

    @RequestMapping("/config/reload")
    public ReloadConfigResponse reload() {
        return config.reload();
    }
}
