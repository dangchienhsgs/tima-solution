package com.tima.solution.controllers;

import com.tima.solution.model.response.FeedbackResponse;
import com.tima.solution.services.feedback.SystemFeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeedbackController {

    private SystemFeedbackService feedbackService;

    @RequestMapping("/feedback")
    public FeedbackResponse feedback(@RequestParam(value = "city", defaultValue = "-1") int city,
                                     @RequestParam(value = "city", defaultValue = "-1") int district) {

        if (city == -1) {
            return new FeedbackResponse(
                    "success",
                    feedbackService.findByDistrictId(district),
                    city,
                    district
            );
        }

        return new FeedbackResponse(
                "success",
                feedbackService.findByCityId(city),
                city,
                district
        );
    }

    @Autowired
    public void setFeedbackService(SystemFeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }
}
