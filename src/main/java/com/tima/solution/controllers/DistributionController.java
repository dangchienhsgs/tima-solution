package com.tima.solution.controllers;

import com.tima.solution.engines.DistributionEngine;
import com.tima.solution.entities.mssql.TblLoanCreditEntity;
import com.tima.solution.model.response.DistributionResponse;
import com.tima.solution.services.loan.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DistributionController {

    private LoanService loanService;

    private DistributionEngine distributionEngine;


    @RequestMapping("/distributionNoPrice")
    public DistributionResponse distribution(@RequestParam(value = "loan") int loanId) {
        TblLoanCreditEntity loanCreditEntity = loanService.findLoanById(loanId);
        return distributionEngine.getLenderLoanNoPrice(loanCreditEntity);
    }

    @RequestMapping("/distributionHasPrice")
    public DistributionResponse distributionZero(@RequestParam(value = "loan") int loanId) {
        TblLoanCreditEntity loanCreditEntity = loanService.findLoanById(loanId);
        return distributionEngine.getLenderLoanHasPrice(loanCreditEntity);
    }

    @RequestMapping("/debug/distributionNoPrice")
    public DistributionResponse distributionNoPriceDebug(
            @RequestParam(value = "type") byte type,
            @RequestParam(value = "city") int city,
            @RequestParam(value = "district") int district,
            @RequestParam(value = "money") long money) {

        TblLoanCreditEntity loan = new TblLoanCreditEntity();
        loan.setId(-1);
        loan.setTypeId(type);
        loan.setCityId(city);
        loan.setDistrictId(district);
        loan.setTotalMoney(money);
        return distributionEngine.getLenderLoanNoPrice(loan);
    }

    @RequestMapping("/debug/distributionHasPrice")
    public DistributionResponse distributionHasPriceDebug(
            @RequestParam(value = "type") byte type,
            @RequestParam(value = "city") int city,
            @RequestParam(value = "district") int district,
            @RequestParam(value = "money") long money) {

        TblLoanCreditEntity loan = new TblLoanCreditEntity();
        loan.setId(-1);
        loan.setTypeId(type);
        loan.setCityId(city);
        loan.setDistrictId(district);
        loan.setTotalMoney(money);
        return distributionEngine.getLenderLoanHasPrice(loan);
    }

    @Autowired
    public void setLoanService(LoanService loanService) {
        this.loanService = loanService;
    }

    @Autowired
    public void setDistributionEngine(DistributionEngine distributionEngine) {
        this.distributionEngine = distributionEngine;
    }
}
