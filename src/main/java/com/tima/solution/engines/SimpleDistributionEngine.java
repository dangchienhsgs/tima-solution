package com.tima.solution.engines;

import com.tima.solution.configs.Constants;
import com.tima.solution.configs.DistributionConfig;
import com.tima.solution.entities.mssql.TblLenderEntity;
import com.tima.solution.entities.mssql.TblLenderLocationEntity;
import com.tima.solution.entities.mssql.TblLoanCreditEntity;
import com.tima.solution.entities.postgresql.DistributionConfigEntity;
import com.tima.solution.entities.postgresql.LenderProperties;
import com.tima.solution.model.response.DistributionResponse;
import com.tima.solution.model.response.LenderDetail;
import com.tima.solution.services.feedback.SystemFeedbackService;
import com.tima.solution.services.lender.LenderService;
import com.tima.solution.services.lenderlocation.LenderLocationService;
import com.tima.solution.services.lenderprop.LenderPropService;
import com.tima.solution.services.push.PushProcessService;
import com.tima.solution.services.spices.ManageSpiceService;
import com.tima.solution.utils.DateUtils;
import com.tima.solution.utils.MapUtils;
import com.tima.solution.utils.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Component
@Primary
public class SimpleDistributionEngine implements DistributionEngine {
    private static Logger logger = Logger.getLogger(SimpleDistributionEngine.class.getName());

    private LenderService lenderService;

    private ManageSpiceService manageSpiceService;

    private LenderPropService lenderPropService;

    private DistributionConfig config;

    private PushProcessService pushProcessService;

    private LenderLocationService lenderLocationService;

    private SystemFeedbackService systemFeedbackService;

    @Override
    public DistributionResponse getLenderLoanNoPrice(TblLoanCreditEntity loan) {
        // vay theo sim
        if (loan.getTypeId() == 18) {
            logger.info("Loan " + loan.getId() + " is a sim type, forward to tima lender");
            return new DistributionResponse("Success", false, Collections.singletonList(convert(lenderService.getTimaLender(), null, Constants.TYPE_TIMA_LENDER)), loan);
        }

        return processNoPrice(loan);
    }

    @Override
    public DistributionResponse getLenderLoanHasPrice(TblLoanCreditEntity loan) {
        if (loan.getTypeId() == 18) {
            logger.info("Loan " + loan.getId() + " is a sim type, forward to tima lender");
            return new DistributionResponse("Success", false, Collections.singletonList(convert(lenderService.getTimaLender(), null, Constants.TYPE_TIMA_LENDER)), loan);
        }

        return processHasPrice(loan);
    }

    private List<TblLenderEntity> filterLenderByType(List<TblLenderEntity> lenders, int type) {
        return lenders.stream()
                .filter(x -> manageSpiceService.isLenderAcceptProductId(x.getId(), type))
                .collect(Collectors.toList());
    }

    private DistributionResponse processNoPrice(TblLoanCreditEntity loan) {
        List<TblLenderEntity> matchLenders = getMatchLender(loan.getCityId(), loan.getDistrictId());
        matchLenders = filterLenderByType(matchLenders, loan.getTypeId());
        systemFeedbackService.checkFilterLocationAndType(loan.getId(), matchLenders.size(), loan.getCityId(), loan.getDistrictId(), loan.getTypeId());

        // filter lender has no money
        matchLenders = matchLenders.stream().filter(x -> x.getMoney() == 0).collect(Collectors.toList());
        systemFeedbackService.checkHasNoPriceLender(loan.getId(), matchLenders.size(), loan.getCityId(), loan.getDistrictId(), loan.getTypeId());

        // filter lender by type
        logger.info("Loan " + loan.getId() + ": " + matchLenders.size() + " lenders match type " + loan.getTypeId());

        // filter lender by number time push per day
        matchLenders = filterLenderByNumberTimePush(matchLenders);
        logger.info("Loan " + loan.getId() + ": " + matchLenders.size() + " lenders with number time push suitable");

        // filter is pushed lenders
        Set<Integer> pushedLenders = pushProcessService.getLenderPushByLoan(loan.getId());
        matchLenders = matchLenders.stream().filter(x -> !pushedLenders.contains(x.getId())).collect(Collectors.toList());
        logger.info("Loan " + loan.getId() + ": " + matchLenders.size() + " lenders not be pushed to loan " + loan.getId());

        List<LenderDetail> lenderDetails = processLender(matchLenders, loan);

        return new DistributionResponse("Success", false, lenderDetails, loan);
    }

    private List<TblLenderEntity> getMatchLender(int city, int district) {
        // filter lender by location
        List<TblLenderLocationEntity> lenderLocations = lenderLocationService.getLenderAcceptLocation(city, district);
        return  lenderService.getLenderById(lenderLocations
                .stream()
                .map(TblLenderLocationEntity::getLenderId)
                .collect(Collectors.toList()));

    }

    private List<TblLenderEntity> filterNewOrHasMoney(List<TblLenderEntity> lenders) {
        return lenders.stream()
                .filter(x -> x.getMoney() > 0 || DateUtils.dayBetween(new Date(x.getCreateDate().getTime()), new Date()) <=2)
                .collect(Collectors.toList());
    }


    private DistributionResponse processHasPrice(TblLoanCreditEntity loan) {
        // filter by location and type
        List<TblLenderEntity> matchLenders = getMatchLender(loan.getCityId(), loan.getDistrictId());
        matchLenders = filterLenderByType(matchLenders, loan.getTypeId());
        systemFeedbackService.checkFilterLocationAndType(loan.getId(), matchLenders.size(), loan.getCityId(), loan.getDistrictId(), loan.getTypeId());

        matchLenders = filterNewOrHasMoney(matchLenders);
        systemFeedbackService.checkHasPriceLender(loan.getId(), matchLenders.size(), loan.getCityId(), loan.getDistrictId(), loan.getTypeId());

        // filter lender by number time push per day
        matchLenders = filterLenderByNumberTimePush(matchLenders);
        logger.info("Loan " + loan.getId() + ": " + matchLenders.size() + " lenders with number time push suitable");

        // filter is pushed lenders
        Set<Integer> pushedLenders = pushProcessService.getLenderPushByLoan(loan.getId());
        matchLenders = matchLenders.stream().filter(x -> !pushedLenders.contains(x.getId())).collect(Collectors.toList());
        logger.info("Loan " + loan.getId() + ": " + matchLenders.size() + " lenders not be pushed to loan " + loan.getId());

        List<LenderDetail> lenderDetails = processLender(matchLenders, loan);

        return new DistributionResponse("Success", false, lenderDetails, loan);
    }

    private List<LenderDetail> processLender(List<TblLenderEntity> lenders, TblLoanCreditEntity loan) {
        // if number lender too small
        if (lenders.size() < config.getConfig().getNumLenderPerTime()) {
            List<LenderDetail> lenderDetails = new ArrayList<>();
            for (TblLenderEntity entity : lenders) {
                lenderDetails.add(convert(entity, null, Constants.TYPE_SMALL_LENDER));
            }

            return lenderDetails;
        }

        List<TblLenderEntity> newLenders = lenders
                .stream()
                .filter(x -> x.getTotalLoanPush() == 0)
                .collect(Collectors.toList());

        // get good and active lender
        List<TblLenderEntity> goodAndActiveLenders = lenders
                .stream()
                .filter(this::isGoodLender)
                .filter(this::isActiveLender)
                .collect(Collectors.toList());

        List<TblLenderEntity> goodAndInactiveLenders = lenders
                .stream()
                .filter(this::isGoodLender)
                .filter(x -> !isActiveLender(x))
                .collect(Collectors.toList());

        List<TblLenderEntity> badAndActiveLenders = lenders
                .stream()
                .filter(x -> !isGoodLender(x))
                .filter(this::isActiveLender)
                .collect(Collectors.toList());

        List<TblLenderEntity> badAndInactiveLenders = lenders
                .stream()
                .filter(x -> !isActiveLender(x))
                .filter(x -> !isGoodLender(x))
                .collect(Collectors.toList());


        DistributionConfigEntity cfg = config.getConfig();
        int numNewLender = (int) Math.round(cfg.getNumLenderPerTime() * cfg.getDistributionNewPercent());
        int numGoodAndActive = (int) Math.round(cfg.getNumLenderPerTime() * cfg.getDistributionGoodAndActivePercent());
        int numGoodAndInactive = (int) Math.round(cfg.getNumLenderPerTime() * cfg.getDistributionGoodAndInactivePercent());
        int numBadAndActive = (int) Math.round(cfg.getNumLenderPerTime() * cfg.getDistributionBadAndActivePercent());
        int numBadAndInactive = (int) Math.round(cfg.getNumLenderPerTime() * cfg.getDistributionBadAndInactivePercent());


        List<TblLenderEntity> choiceNewLenders = choiceNewLender(newLenders, numNewLender);
        logger.info("Loan " + loan.getId() + ": number new lender: " + choiceNewLenders.size() + " / " + numNewLender);

        List<TblLenderEntity> choiceGoodAndActiveLenders = choiceGoodAndActive(goodAndActiveLenders, numGoodAndActive);
        logger.info("Loan " + loan.getId() + ": number good and active lender: " + choiceGoodAndActiveLenders.size() + " / " + numGoodAndActive);

        List<TblLenderEntity> choiceGoodAndInactiveLenders = choiceGoodAndInactive(goodAndInactiveLenders, numGoodAndInactive);
        logger.info("Loan " + loan.getId() + ": number good and inactive lender: " + choiceGoodAndInactiveLenders.size() + " / " + numGoodAndInactive);

        List<TblLenderEntity> choiceBadAndActiveLenders = choiceBadAndActive(badAndActiveLenders, loan, numBadAndActive);
        logger.info("Loan " + loan.getId() + ": number bad and active lender: " + choiceBadAndActiveLenders.size() + " / " + numBadAndActive);

        List<TblLenderEntity> choiceBadAndInactiveLenders = choiceBadAndInactive(badAndInactiveLenders, loan, numBadAndInactive);
        logger.info("Loan " + loan.getId() + ": number bad and inactive lender: " + choiceBadAndInactiveLenders.size() + " / " + numBadAndInactive);


        List<LenderDetail> lenderDetails = new ArrayList<>();
        for (TblLenderEntity lenderEntity : choiceNewLenders) {
            LenderDetail newLender = convert(lenderEntity, lenderPropService.getLenderProp(lenderEntity.getId()), Constants.TYPE_NEW_LENDER);
            lenderDetails.add(newLender);
        }

        for (TblLenderEntity lenderEntity : choiceGoodAndActiveLenders) {
            LenderDetail newLender = convert(lenderEntity, lenderPropService.getLenderProp(lenderEntity.getId()), Constants.TYPE_GOOD_AND_ACTIVE_LENDER);
            lenderDetails.add(newLender);
        }

        for (TblLenderEntity lenderEntity : choiceGoodAndInactiveLenders) {
            LenderDetail newLender = convert(lenderEntity, lenderPropService.getLenderProp(lenderEntity.getId()), Constants.TYPE_GOOD_AND_INACTIVE_LENDER);
            lenderDetails.add(newLender);
        }

        for (TblLenderEntity lenderEntity : choiceBadAndActiveLenders) {
            LenderDetail newLender = convert(lenderEntity, lenderPropService.getLenderProp(lenderEntity.getId()), Constants.TYPE_BAD_AND_ACTIVE_LENDER);
            lenderDetails.add(newLender);
        }

        for (TblLenderEntity lenderEntity : choiceBadAndInactiveLenders) {
            LenderDetail newLender = convert(lenderEntity, lenderPropService.getLenderProp(lenderEntity.getId()), Constants.TYPE_BAD_AND_INACTIVE_LENDER);
            lenderDetails.add(newLender);
        }

        return lenderDetails;
    }

    private LenderDetail convert(TblLenderEntity entity, LenderProperties prop, String type) {
        LenderDetail lenderDetail = new LenderDetail();
        lenderDetail.setCityId(entity.getCityId());
        lenderDetail.setDistrictId(entity.getDistrictId());
        lenderDetail.setLenderId(entity.getId());
        lenderDetail.setType(type);
        lenderDetail.setTotalMoney(entity.getMoney());
        lenderDetail.setCreatedDate(entity.getCreateDate());
        lenderDetail.setActive(isActiveLender(prop));
        lenderDetail.setGood(isGoodLender(entity));

        if (prop != null) {
            logger.info("Lender " + entity.getId() + " with createdDate= " + entity.getCreateDate() + " is not have prop");
            lenderDetail.setA3Rate(prop.getA3Rate());
            lenderDetail.setA7Rate(prop.getA7Rate());
        } else {
            // not found in props, return 0 for active
            lenderDetail.setA3Rate(0);
            lenderDetail.setA7Rate(0);
        }

        if (StringUtils.equals(Constants.TYPE_NEW_LENDER, type)) {
            return lenderDetail;
        }

        lenderDetail.setAcceptRate(getAcceptRate(entity));
        return lenderDetail;
    }

    private boolean isActiveLender(TblLenderEntity lenderEntity) {
        LenderProperties propsEntity = lenderPropService.getLenderProp(lenderEntity.getId());
        return isActiveLender(propsEntity);
    }

    private boolean isActiveLender(LenderProperties properties) {
        if (properties == null) {
            logger.info("Can not find props of lender " + properties.getId());
            return false;
        }

        return properties.getA3Rate() >= Constants.LENDER_MIN_A3_ACTIVE || properties.getA7Rate() >= Constants.LENDER_MIN_A7_ACTIVE;
    }


    private List<TblLenderEntity> choiceGoodAndActive(List<TblLenderEntity> lenders, int numChoice) {
        // choice good and active

        if (lenders.size() <= numChoice) {
            return lenders;
        }

        Map<TblLenderEntity, Double> scoreMap = new HashMap<>();

        for (TblLenderEntity entity : lenders) {
            LenderProperties prop = lenderPropService.getLenderProp(entity.getId());

            if (prop == null) {
                logger.info("Can not find prop of lender " + entity.getId());
                continue;
            }


            // accept more to be choose for other accept
            // max value can get more loan
            double score = prop.getMaxAcceptValue() * 1.0 / (prop.getNumAcceptLoan() + 1);

            logger.info("Score lender " + entity.getId() + ": " + score);
            scoreMap.put(entity, score);
        }

        return MapUtils.getTop(scoreMap, numChoice);
    }

    private List<TblLenderEntity> choiceGoodAndInactive(List<TblLenderEntity> lenders, int numChoice) {
        // choice like good and active
        return choiceGoodAndActive(lenders, numChoice);
    }

    private List<TblLenderEntity> choiceNewLender(List<TblLenderEntity> lenders, int numChoice) {
        return NumberUtils.choice(lenders, numChoice);
    }

    private List<TblLenderEntity> choiceBadAndActive(List<TblLenderEntity> lenders, TblLoanCreditEntity loan, int numChoice) {
        // bad and active
        // loan match with max accept

        Map<TblLenderEntity, Double> scores = new HashMap<>();

        for (TblLenderEntity entity : lenders) {
            LenderProperties prop = lenderPropService.getLenderProp(entity.getId());

            if (prop == null) {
                logger.info("Can not find prop of lender " + entity.getId());
                continue;
            }

            double score;

            if (prop.getMaxAcceptValue() != null) {
                score = Math.abs(loan.getTotalMoney() - prop.getMaxAcceptValue()) * 1.0 / loan.getTotalMoney();
            } else {
                score = Math.abs((prop.getA3Rate() + 1) * 1.0 / 3 + (prop.getA7Rate() + 1) * 1.0 / 7);
            }

            logger.info("Score lender " + entity.getId() + ": " + score);
            scores.put(entity, score);
        }

        return MapUtils.getTop(scores, numChoice);
    }

    private List<TblLenderEntity> choiceBadAndInactive(List<TblLenderEntity> lenders, TblLoanCreditEntity loan, int numChoice) {
        // choice like bad and active
        return NumberUtils.choice(lenders, numChoice);
    }


    /**
     * @return
     */
    private boolean isGoodLender(TblLenderEntity lenderEntity) {
        if (lenderEntity.getTotalLoanPush() == 0) {
            return false;
        }

        return lenderEntity.getLoanAccept() * 1.0 / lenderEntity.getTotalLoanPush() >= 0.2;
    }

    private double getAcceptRate(TblLenderEntity lenderEntity) {
        return lenderEntity.getLoanAccept() * 1.0 / lenderEntity.getTotalLoanPush();
    }

    /**
     * Filter if a lender push exceed number time /day then it will be excluded
     * TODO
     *
     * @param lenders
     * @return
     */
    private List<TblLenderEntity> filterLenderByNumberTimePush(Iterable<TblLenderEntity> lenders) {
        List<TblLenderEntity> chosenLenders = new ArrayList<>();
        for (TblLenderEntity entity : lenders) {
            chosenLenders.add(entity);
        }

        return chosenLenders;
    }

    @Autowired
    public void setLenderService(LenderService lenderService) {
        this.lenderService = lenderService;
    }

    @Autowired
    public void setManageSpiceService(ManageSpiceService manageSpiceService) {
        this.manageSpiceService = manageSpiceService;
    }

    @Autowired
    public void setLenderPropService(LenderPropService lenderPropService) {
        this.lenderPropService = lenderPropService;
    }

    @Autowired
    public void setPushProcessService(PushProcessService pushProcessService) {
        this.pushProcessService = pushProcessService;
    }

    @Autowired
    public void setConfig(DistributionConfig config) {
        this.config = config;
    }

    @Autowired
    public void setLenderLocationService(LenderLocationService lenderLocationService) {
        this.lenderLocationService = lenderLocationService;
    }

    @Autowired
    public void setSystemFeedbackService(SystemFeedbackService systemFeedbackService) {
        this.systemFeedbackService = systemFeedbackService;
    }
}
