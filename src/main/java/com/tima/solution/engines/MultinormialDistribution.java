package com.tima.solution.engines;

import java.util.Random;

public class MultinormialDistribution {

    private static Random generator = new Random();
    private double[] distribution;
    private int range;

    //Constructor
    public MultinormialDistribution(double[] probabilities) {
        range = probabilities.length;
        distribution = new double[range];
        double sumProb = 0;
        for (double value : probabilities) {
            sumProb += value;
        }
        double position = 0;
        for (int i = 0; i < range; ++i) {
            position += probabilities[i] / sumProb;
            distribution[i] = position;
        }
        distribution[range - 1] = 1.0;
    }

    public static void main(String args[]) {
        MultinormialDistribution distribution = new MultinormialDistribution(new double[]{0.2, 0.8});
    }

    public int sample() {
        double uniform = generator.nextDouble();
        for (int i = 0; i < range; ++i) {
            if (uniform < distribution[i]) {
                return i;
            }
        }
        return range - 1;
    }
}