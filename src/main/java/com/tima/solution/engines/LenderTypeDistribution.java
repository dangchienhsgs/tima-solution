package com.tima.solution.engines;

import com.tima.solution.services.lenderprop.LenderType;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class LenderTypeDistribution implements LenderDistribution {

    private static double newScore = 0.2;
    private static double goodAndActiveScore = 0.3;
    private static double goodAndInactiveScore = 0.2;
    private static double badAndActiveScore = 0.2;
    private static double badAndInactiveScore = 0.1;
    private static double probabilities[] = new double[]{
            newScore,
            badAndActiveScore,
            badAndInactiveScore,
            goodAndActiveScore,
            goodAndInactiveScore
    };
    private List<LenderType> lenderTypes = Arrays.asList(
            LenderType.NEW,
            LenderType.BAD_AND_ACTIVE,
            LenderType.BAD_AND_INACTIVE,
            LenderType.GOOD_AND_ACTIVE,
            LenderType.GOOD_AND_INACTIVE
    );
    private MultinormialDistribution distribution;

    public LenderTypeDistribution() {
        distribution = new MultinormialDistribution(probabilities);
    }

    @Override
    public LenderType getLenderType(int loandId) {
        return lenderTypes.get(distribution.sample());
    }
}
