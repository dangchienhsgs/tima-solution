package com.tima.solution.engines;

import com.tima.solution.entities.mssql.TblLoanCreditEntity;
import com.tima.solution.model.response.DistributionResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class LargeDistributionEngine implements DistributionEngine {
    @Override
    public DistributionResponse getLenderLoanHasPrice(TblLoanCreditEntity loan) {
        return null;
    }

    @Override
    public DistributionResponse getLenderLoanNoPrice(TblLoanCreditEntity loan) {
        return null;
    }
}
