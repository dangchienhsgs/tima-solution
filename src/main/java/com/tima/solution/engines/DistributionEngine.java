package com.tima.solution.engines;

import com.tima.solution.entities.mssql.TblLoanCreditEntity;
import com.tima.solution.model.response.DistributionResponse;
import org.springframework.context.annotation.Configuration;

@Configuration
public interface DistributionEngine {
    DistributionResponse getLenderLoanHasPrice(TblLoanCreditEntity loan);
    DistributionResponse getLenderLoanNoPrice(TblLoanCreditEntity loan);

}
