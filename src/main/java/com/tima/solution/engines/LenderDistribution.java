package com.tima.solution.engines;

import com.tima.solution.services.lenderprop.LenderType;

public interface LenderDistribution {
    LenderType getLenderType(int loandId);
}
