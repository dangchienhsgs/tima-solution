package com.tima.solution.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class NumberUtils {
    public static <T> List<T> shuffleList(List<T> elems) {

        List<T> copyList = new ArrayList<>(elems);

        int n = elems.size();
        Random random = new Random();
        random.nextInt();
        for (int i = 0; i < n; i++) {
            int change = i + random.nextInt(n - i);
            swap(copyList, i, change);
        }

        return copyList;
    }

    public static <T> void swap(List<T> a, int i, int change) {
        T helper = a.get(i);
        a.set(i, a.get(change));
        a.set(change, helper);
    }

    public static <T> List<T> choice(List<T> elems, int choiceSize) {
        if (elems.size() < choiceSize) {
            return elems;
        }

        return shuffleList(elems).subList(0, choiceSize);
    }

    public static void main(String args[]) {
        System.out.println(choice(Arrays.asList(1, 2, 3, 4, 5), 2));
    }
}
