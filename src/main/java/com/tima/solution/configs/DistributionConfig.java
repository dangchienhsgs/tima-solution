package com.tima.solution.configs;

import com.tima.solution.entities.postgresql.DistributionConfigEntity;
import com.tima.solution.model.response.ReloadConfigResponse;
import com.tima.solution.repositories.postgres.DistributionConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class DistributionConfig {
    private static Logger logger = Logger.getLogger(DistributionConfig.class.getName());
    private static int defaultConfig = 1;
    private DistributionConfigEntity entity;
    private DistributionConfigRepository repository;

    @Autowired
    public DistributionConfig(DistributionConfigRepository repository) {
        this.repository = repository;

        entity = repository.findOne(defaultConfig);
        logger.info("Load config: " + entity);
    }

    public DistributionConfigEntity getConfig() {
        return entity;
    }

    public ReloadConfigResponse reload() {
        DistributionConfigEntity backupEnity = repository.findOne(defaultConfig);
        if (backupEnity != null) {
            logger.info("Reload config: " + backupEnity);
            entity = backupEnity;
            return new ReloadConfigResponse("Reload config success", false, entity);
        } else {
            return new ReloadConfigResponse("Reload config fail, keep current config", false, entity);
        }
    }
}
