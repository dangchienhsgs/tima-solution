package com.tima.solution.configs;

public class Constants {

    public static final int LENDER_MIN_A3_ACTIVE = 1;
    public static final int LENDER_MIN_A7_ACTIVE = 3;

    public static final String TYPE_NEW_LENDER = "new_lender";
    public static final String TYPE_GOOD_AND_ACTIVE_LENDER = "good_and_active_lender";
    public static final String TYPE_GOOD_AND_INACTIVE_LENDER = "good_and_inactive_lender";
    public static final String TYPE_BAD_AND_ACTIVE_LENDER = "bad_and_active_lender";
    public static final String TYPE_BAD_AND_INACTIVE_LENDER = "bad_and_inactive_lender";
    public static final String TYPE_SMALL_LENDER = "small_lender";
    public static final String TYPE_TIMA_LENDER = "tima_lender";

    public static final int PRIORITY_HIGH = 3;
    public static final int PRIORITY_MEDIUM = 2;
    public static final int PRIORITY_LOW = 1;
}
