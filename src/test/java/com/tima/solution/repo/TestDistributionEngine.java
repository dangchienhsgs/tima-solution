package com.tima.solution.repo;

import com.tima.solution.engines.DistributionEngine;
import com.tima.solution.entities.mssql.TblLoanCreditEntity;
import com.tima.solution.model.response.DistributionResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDistributionEngine {

    @Autowired
    private DistributionEngine distributionEngine;

    @Test
    public void test() {
        TblLoanCreditEntity loanCreditEntity = new TblLoanCreditEntity();
        loanCreditEntity.setCityId(1);
        loanCreditEntity.setDistrictId(1);
        loanCreditEntity.setTypeId((byte) 6);
        loanCreditEntity.setTotalMoney(2500000L);
        loanCreditEntity.setId(2);
        DistributionResponse response = distributionEngine.getLenderLoanHasPrice(loanCreditEntity);
        System.out.println(response.getData().size());
    }
}
