package com.tima.solution.repo;

import com.tima.solution.utils.DateUtils;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TestDateUtils {

    @Test
    public void test() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy");
        Assert.assertEquals(DateUtils.dayBetween(
                dateFormat.parse("04_03_2018"),
                dateFormat.parse("05_03_2018")
        ), 1);
    }
}
